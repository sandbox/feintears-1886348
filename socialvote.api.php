<?php
/**
 * @file
 * API
 */

/**
 * Hook for record to log.
 */
function hook_socialvote_vote($svid, $vid, $version, $user_id, $user_soc_id, $user_name) {
  global $user;
  db_insert('socialvote_log')
  ->fields(array(
    'svid' => $svid,
    'vid' => $vid,
    'version' => $version,
    'user_id' => $user_id,
    'user_soc_id' => $user_soc_id,
    'user_name' => $user_name,
    'user_ip' => $user->hostname,
    'timestamp' => REQUEST_TIME,
  ))
  ->execute();
}
