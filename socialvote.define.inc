<?php
/**
 * @file
 * Socialvote options.
 * You can change this file for performance.
 */

define('SOCIALVOTE_VK_APPID', variable_get('socialvote_vk_appid', '3078269'));
define('SOCIALVOTE_VK_SECRET', variable_get('socialvote_vk_secret', 'TgQQJfGW30JawjO2rtOv'));
define('SOCIALVOTE_FB_APPID', variable_get('socialvote_fb_appid', '374078819331511'));
define('SOCIALVOTE_FB_SECRET', variable_get('socialvote_fb_secret', '2fcfeb23b4708fe0e185974537c40cb3'));

define('SOCIALVOTE_PAGELINK_ADDVOTE', variable_get('socialvote_pagelink_addvote', 'addvote'));
$pagelink_all = variable_get('socialvote_pagelink_all', '');
if ($pagelink_all != '') {
  define('SOCIALVOTE_PAGELINK_ALL', $pagelink_all);
}
$pagelink_publish = variable_get('socialvote_pagelink_publish', 'publish');
if ($pagelink_publish != '') {
  define('SOCIALVOTE_PAGELINK_PUBLISH', $pagelink_publish);
}
$pagelink_notpublish = variable_get('socialvote_pagelink_notpublish', 'notpublish');
if ($pagelink_notpublish != '') {
  define('SOCIALVOTE_PAGELINK_NOTPUBLISH', $pagelink_notpublish);
}

$GLOBALS['SOCIALVOTE_PAGETITLE_ADDVOTE'] = variable_get('socialvote_pagetitle_addvote', 'Add vote');
define('SOCIALVOTE_PAGETITLE_ADDVOTE', t($GLOBALS['SOCIALVOTE_PAGETITLE_ADDVOTE']));
if ($pagelink_all != '') {
  $GLOBALS['SOCIALVOTE_PAGETITLE_ALL'] = variable_get('socialvote_pagetitle_all', 'All votes');
  define('SOCIALVOTE_PAGETITLE_ALL', t($GLOBALS['SOCIALVOTE_PAGETITLE_ALL']));
}
if ($pagelink_publish != '') {
  $GLOBALS['SOCIALVOTE_PAGETITLE_PUBLISH'] = variable_get('socialvote_pagetitle_publish', 'Publish votes');
  define('SOCIALVOTE_PAGETITLE_PUBLISH', t($GLOBALS['SOCIALVOTE_PAGETITLE_PUBLISH']));
}
if ($pagelink_notpublish != '') {
  $GLOBALS['SOCIALVOTE_PAGETITLE_NOTPUBLISH'] = variable_get('socialvote_pagetitle_notpublish', 'Not publish votes');
  define('SOCIALVOTE_PAGETITLE_NOTPUBLISH', t($GLOBALS['SOCIALVOTE_PAGETITLE_NOTPUBLISH']));
}

define('SOCIALVOTE_VERSION_SPR1', '<&>'); define('SOCIALVOTE_SECTION_SPR1', '<&>');
define('SOCIALVOTE_VERSION_SPR2', '<*>'); define('SOCIALVOTE_SECTION_SPR2', '<*>');

$sections = variable_get('socialvote_sections', 'Section 1' . SOCIALVOTE_SECTION_SPR1 . 'sect1' . SOCIALVOTE_SECTION_SPR2 . 'Section 2' . SOCIALVOTE_SECTION_SPR1 . 'sect2' . SOCIALVOTE_SECTION_SPR2 . 'Section 3' . SOCIALVOTE_SECTION_SPR1 . 'sect3');
$sections_temp = explode(SOCIALVOTE_SECTION_SPR2, $sections);
$GLOBALS['_socialvote_sections_en'] = array();
$GLOBALS['_socialvote_sections'] = array();
for ($i = 1; $i <= count($sections_temp); $i++) {
  $sect = explode(SOCIALVOTE_SECTION_SPR1, $sections_temp[$i - 1]);
  $GLOBALS['_socialvote_sections_en'][$i] = array('name' => $sect[0], 'url' => $sect[1]);
  $GLOBALS['_socialvote_sections'][$i] = array('name' => t($sect[0]), 'url' => $sect[1]);
}

define('SOCIALVOTE_SECTION_X', variable_get('socialvote_section_x', 3));
define('SOCIALVOTE_SECTION_Y', variable_get('socialvote_section_y', 2));
define('SOCIALVOTE_MAXVERSIONS', 10);
define('SOCIALVOTE_CAREFUL_INSPECTION', 50);
define('SOCIALVOTE_LOADPORTION', 5);
// Display 1 images at left from current.
define('SOCIALVOTE_DISPLAYPORTION_LEFT', 1);
// Display 3 images at right from current.
define('SOCIALVOTE_DISPLAYPORTION_RIGHT', 2);
// Length of field in database.
define('SOCIALVOTE_VERSION_LENGTH', 500);

define('SOCIALVOTE_S_CORE', '000');
define('SOCIALVOTE_S_VK', 'vk');
define('SOCIALVOTE_S_FB', 'fb');

define('SOCIALVOTE_ALL', '2');
define('SOCIALVOTE_PUBLISH', '1');
define('SOCIALVOTE_NOTPUBLISH', '0');

define('SOCIALVOTE_VOTE_RESPONSE_OK', 100);
// Incorrect id of vote or version.
define('SOCIALVOTE_VOTE_RESPONSE_ERROR1', 1);
// Error of social authorization.
define('SOCIALVOTE_VOTE_RESPONSE_ERROR2', 2);
// This user already vote.
define('SOCIALVOTE_VOTE_RESPONSE_ERROR3', 3);
