/**
 * @file
 *
 * For design of socialvotes panel.
 */

//class Socialvote
function socialvote(svid, body, description, versions, versions_count, imagefile, author_uid, author_soc_id, author_name, moderation, section, api_id, created, changed) {
  var that = this;

  this.svid = svid;
  this.body = body.replace(/<br ?\/?>/g, "");
  this.description = description.replace(/<br ?\/?>/g, "");
  this.versions = versions;
  //don't use it (use count(versions) )
  this.versions_count = versions_count;
  this.imagefile = imagefile; this.getimagefile = function() {return sv_domain + '/' + that.imagefile.split('public://').join(sv_files_path + '/'); };
  this.author_uid = author_uid;
  this.author_soc_id = author_soc_id;
  this.author_name = author_name;
  this.moderation = moderation;
  this.section = section;
  this.api_id = api_id;
  this.created = created;
  this.changed = changed;

  this.v_arr = [];
  this.init = function() {
    v_arr_temp = versions.split(sv_spr2);
    for(ind in v_arr_temp) {
      v_temp = v_arr_temp[ind].split(sv_spr1);
      this.v_arr.push(new socialvote_version(v_temp[0], v_temp[1]));
    }
  }
  this.init();

  this.uri = function() {
    return sv_domain + '/' + that.uripath();
  }
  this.uripath = function() {
    if(sv_pagelink_publish != '' && that.moderation == 1) {
      return sv_pagelink_publish + '/' + sv_section_url + '/' + that.svid;
    }
    if(sv_pagelink_notpublish != '' && that.moderation == 0) {
      return sv_pagelink_notpublish + '/' + sv_section_url + '/' + that.svid;
    }
    if(sv_pagelink_all != '') {
      return sv_pagelink_all + '/' + sv_section_url + '/' + that.svid;
    }
    return '';
  }

  socialvote.fromObj = function(obj) {
    return new socialvote(
      obj.svid,
      obj.body,
      obj.description,
      obj.versions,
      obj.versions_count,
      obj.imagefile,
      obj.author_uid,
      obj.author_soc_id,
      obj.author_name,
      obj.moderation,
      obj.section,
      obj.api_id,
      obj.created,
      obj.changed
    );
  }
}

//class Version
function socialvote_version(text, votes) {
  this.text = text;
  this.votes = votes;
}

/*
 * Navigation of socialvotes
 */

function socialvote_indexof(svid, arr) {
  for(i in arr) {
    if(arr[i].svid == svid) {
      return i;
    }
  }
  return -1;
};

//#sv_left a
function socialvote_left() {
  //current sv
  cur_index = socialvote_indexof(sv_cur.svid, svs);
  socialvote_current(svs[ --cur_index ]);
  socialvote_left_load(cur_index);
}
function socialvote_left_load(cur_index) {
  jQuery('#sv_right').css('visibility', 'visible');
  if(cur_index == 0) {
    jQuery('#sv_left').css('visibility', 'hidden');

    //loading...
    if(sv_is_left) {
      jQuery('#sv_left_loading').css('visibility', 'visible');
      socialvote_services_load(0, 1, sv_load, sv_section_id, svs[0].svid, function(data, textStatus, jqXHR) {
        jQuery('#sv_left_loading').css('visibility', 'hidden');

        //parse to sv-array
        var resp = jQuery.parseJSON(data);
        for(i in resp) {
          if(i != 'more' && resp[i].svid != sv_cur.svid) {
            svs.splice(0, 0, socialvote.fromObj(resp[i]));
          }
        }
        sv_is_left = (resp.more == "yes");

        jQuery('#sv_left').css('visibility', 'visible');
        socialvote_current_imagenav(sv_cur);
      },
      function(jqXHR, textStatus, errorThrown) {
        jQuery('#sv_left_loading').css('visibility', 'hidden');
        socialvote_connect_error(jqXHR, textStatus, errorThrown);
      });
    }
  }
}

//#sv_right a
function socialvote_right() {
  //current sv
  cur_index = socialvote_indexof(sv_cur.svid, svs);
  socialvote_current(svs[++cur_index]);
  socialvote_right_load(cur_index);
}
function socialvote_right_load(cur_index) {
  jQuery('#sv_left').css('visibility', 'visible');
  if(cur_index == svs.length - 1) {
    jQuery('#sv_right').css('visibility', 'hidden');

    //loading...
    if(sv_is_right) {
      jQuery('#sv_right_loading').css('visibility', 'visible');
      socialvote_services_load(0, 0, sv_load, sv_section_id, svs[svs.length - 1].svid, function(data, textStatus, jqXHR) {
        jQuery('#sv_right_loading').css('visibility', 'hidden');

        //parse to sv-array
        var resp = jQuery.parseJSON(data);
        var old_count = svs.length;
        for(i in resp) {
          if(i != 'more' && resp[i].svid != sv_cur.svid) {
            svs.splice(old_count, 0, socialvote.fromObj(resp[i]));
          }
        }
        sv_is_right = (resp.more == "yes");

        jQuery('#sv_right').css('visibility', 'visible');
        socialvote_current_imagenav(sv_cur);
      },
      function(jqXHR, textStatus, errorThrown) {
        jQuery('#sv_right_loading').css('visibility', 'hidden');
        socialvote_connect_error(jqXHR, textStatus, errorThrown);
      });
    }
  }
}

//update #sv
function socialvote_current_svid(svid) {
  cur_index = socialvote_indexof(svid, svs);
  socialvote_current(svs[cur_index]);
  if(cur_index == 0) {
    socialvote_left_load(cur_index);
  } else {
    jQuery('#sv_left').css('visibility', 'visible');
  }
  if(cur_index == svs.length - 1) {
    socialvote_right_load(cur_index);
  } else {
    jQuery('#sv_right').css('visibility', 'visible');
  }
}
function socialvote_current(sv) {
  sv_cur = sv;
  social.comments('#sv_comments .sv_comments', sv_cur.uri());

  //history
  history.replaceState({}, '', '/' + sv_cur.uripath());

  //update #sv .imagenav
  socialvote_current_imagenav(sv_cur);

  //update #sv .vote
  jQuery('#sv .vote .svrep_svid').text(sv_cur.svid);
  jQuery('#sv .vote .svrep_body').text(sv_cur.body.replace(/<br ?\/?>/g, ""));
  jQuery('#sv .vote .svrep_description').text(sv_cur.description.replace(/<br ?\/?>/g, ""));
  jQuery('#sv .vote .svrep_versions').text(sv_cur.versions);
  jQuery('#sv .vote .svrep_versions_count').text(sv_cur.versions_count);
  jQuery('#sv .vote .svrep_imagefile').attr("src", sv_cur.getimagefile());
  jQuery('#sv .vote .svrep_author_uid').text(sv_cur.author_uid);
  jQuery('#sv .vote .svrep_author_soc_id').text(sv_cur.author_soc_id);
  jQuery('#sv .vote .svrep_author_name').text(sv_cur.author_name);
  jQuery('#sv .vote .svrep_moderation').text(sv_cur.moderation);
  jQuery('#sv .vote .svrep_api_id').text(sv_cur.api_id);
  jQuery('#sv .vote .svrep_created').text(sv_cur.created);
  jQuery('#sv .vote .svrep_changed').text(sv_cur.changed);

  //update #sv .versions
  vers_templ = jQuery('.sv_vers_templ').html();
  vers = jQuery('#sv .versions');
  vers.text('');
  for(ind in sv.v_arr) {
    vers.append(vers_templ);
    last = jQuery('#sv .versions .vn').last();
    jQuery('.svrep_text', last).text(sv.v_arr[ind].text);
    jQuery('.svrep_votes', last).text(sv.v_arr[ind].votes);
    var a = jQuery('.vote', last);
    a.attr('svid', sv.svid);
    a.attr('vid', ind);
  }

  //update "I like this"
  socialvote_services_generate(sv_cur.svid * 100 + 1, function(data, textStatus, jqXHR) {
    var resp = jQuery.parseJSON(data);
    sv_gen_id = resp[0];
    jQuery('#sv_likeit').text(resp[1][0]);
    jQuery('#sv_dontlikeit').text(resp[1][1]);
  });
}

//update #sv .imagenav
function socialvote_current_imagenav(sv) {
  imgs = jQuery('#sv .imagenav');
  imgs.text('');

  ind = socialvote_indexof(sv.svid, svs);

  for(i = ind - sv_displayportion_left; i < parseInt(ind) + parseInt(sv_displayportion_right) + 1; i++) {
    if(i < 0 || i >= svs.length) {
      imgs.append(jQuery('.sv_img_templ_curno_wl').html());
      img = jQuery('#sv .imagenav .sv_img .svrep_imagefile').last();
      img.attr("src", sv_module_path + '/images/transparent.png');
    }
    else {
      if(svs[i].svid == sv.svid) {
        imgs.append(jQuery('.sv_img_templ_curyes').html());
      }
      else {
        imgs.append(jQuery('.sv_img_templ_curno').html());
      }
      //current and not current (both cases)
      img = jQuery('#sv .imagenav .sv_img .svrep_imagefile').last();
      img.attr('src', svs[i].getimagefile());
      a = jQuery('#sv .imagenav .sv_img .svrep_a').last();
      a.attr('svid', svs[i].svid);
    }
  }
}

/*
 * Vote
 */
function socialvote_vote(svid, vid) {
  if(user_islogin) {
    socialvote_vote2(svid, vid);
  }
  else {
    onlogin_svid = svid;
    onlogin_vid = vid;
    sv_openlogin();
  }
}
function socialvote_vote2(svid, vid) {
  var sv = svs[ socialvote_indexof(svid, svs) ];

  socialvote_loading_start();
  social.getSocById(user_soc_id).wallpost(sv.body, sv.v_arr[vid].text, sv.uri(), sv.getimagefile(), function(r) {
    socialvote_services_vote(svid, vid, function(data, textStatus, jqXHR) {
      switch(data) {
        case '1':
          socialvote_message_open(sv_vote_error1);
          break;

        case '2':
          socialvote_message_open(sv_vote_error2);
          break;

        case '3':
          socialvote_message_open(sv_vote_error3);
          break;

        case '100':
          socialvote_vote_plus(svid, vid);
          socialvote_current(sv);
          if (typeof window['socialvote_updvote'] == 'function') {
            socialvote_updvote();
          }
          socialvote_message_open(sv_vote_thanks);
          break;

        default:
          socialvote_message_open('Unknown error: ' + data);
      }
      socialvote_loading_stop();
    });
  });
}
function socialvote_vote_plus(svid, vid) {
  var ind = socialvote_indexof(svid, svs);
  svs[ind].v_arr[vid].votes++;
}
function sv_openlogin() {
  socialvote_message_open(jQuery('#sv_fixedlogin').html());
}

/*
 * Vote generate
 */
function socialvote_vote_generate(svid, vid, text, url, img) {
  if(user_islogin) {
    socialvote_vote_generate2(svid, vid, text, url, img);
  }
  else {
    onlogin_svid = svid;
    onlogin_vid = vid;
    onlogin_text = text;
    onlogin_url = url;
    onlogin_img = img;
    sv_openlogin();
  }
}
function socialvote_vote_generate2(svid, vid, text, url, img) {
  socialvote_loading_start();
  social.getSocById(user_soc_id).wallpost_likeit(text, url, img, function(r) {
    socialvote_services_vote(svid, vid, function(data, textStatus, jqXHR) {;
      switch(data) {
        case '1':
          socialvote_message_open(sv_vote_error1);
          break;

        case '2':
          socialvote_message_open(sv_vote_error2);
          break;

        case '3':
          socialvote_message_open(sv_vote_error3);
          break;

        case '100':
          socialvote_current(sv_cur);
          socialvote_message_open(sv_vote_thanks);
          break;

        default:
          socialvote_message_open('Unknown error: ' + data);
      }
      socialvote_loading_stop();
    });
  });
}
