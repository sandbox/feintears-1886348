/**
 * @file
 *
 * Contains sVK (vk class).
 */

function sVK(app_id, onLogin, onLogout, onInit) {
  var that = this;
  this.settings = 5;
  this.app_id = app_id;
  this.onLogin = onLogin;
  this.onLogout = onLogout;
  this.onInit = onInit;
  this.orig = null;
  this.init = function(onInit) {
    jQuery.getScript('http://vkontakte.ru/js/api/openapi.js', function() {
    that.orig = VK;
    VK.init({
      apiId: app_id
    });
    VK.Auth.getLoginStatus(authInfo);
      onInit();
    });
  }
  this.login = function() {
    VK.Auth.login(authLogin, that.settings);
  }
  this.logout = function() {
    VK.Auth.logout(authLogout);
  }
  this.wallpost = function(vote_text, version_text, url, img_src, success) {
    VK.Api.call('photos.getWallUploadServer', {}, function(r) {
      if(r.error) { that.error(r.error); }
      if(r.response) {
        socialvote_services_postfile(img_src, r.response.upload_url, function(data, textStatus, jqXHR) {
          var resp = jQuery.parseJSON(data);
          resp = jQuery.parseJSON(resp.response);
          VK.Api.call('photos.saveWallPhoto', {server: resp.server, photo: resp.photo, hash: resp.hash}, function(r) {

            var text = sv_vote_templ;
            text = text.split('%vote%').join(vote_text);
            text = text.split('%version%').join(version_text);
            VK.Api.call('wall.post', {message: text, attachments: r.response[0].id + ',' + url}, function(r) {
              if(r.response) {
                success(r.response);
              }
              if(r.error) {
                socialvote_loading_stop();
                that.error(r.error);
              }
            });

          });
        });
      }
    });
  }
  this.wallpost_likeit = function(text, url, img_src, success) {
    VK.Api.call('photos.getWallUploadServer', {}, function(r) {
      if(r.error) { that.error(r.error); }
      if(r.response) {
        socialvote_services_postfile(img_src, r.response.upload_url, function(data, textStatus, jqXHR) {
          var resp = jQuery.parseJSON(data);
          resp = jQuery.parseJSON(resp.response);
          VK.Api.call('photos.saveWallPhoto', {server: resp.server, photo: resp.photo, hash: resp.hash}, function(r) {

            VK.Api.call('wall.post', {message: text, attachments: r.response[0].id + ',' + url}, function(r) {
              if(r.response) {
                success(r.response);
              }
              if(r.error) {
                socialvote_loading_stop();
                that.error(r.error);
              }
            });

          });
        });
      }
      });
  }
  this.comments = function(comments_selector, url) {
    jQuery(comments_selector).html('<div id="vk_comments"></div>');
    that.orig.Widgets.Comments("vk_comments", {limit: 10, width: "496"}, url);
  }
  this.error = function(error) {
    if(error.error_code != '10007') {
      socialvote_message_open('vk.com error: ' + error.error_msg);
    }
  }

  function authInfo(response) {
    if (response.session) {
      var id = response.session.mid;
      var vk_id = jQuery.cookie('vk_id');
      if(id == vk_id) {
        var vk_firstname = jQuery.cookie('vk_firstname');
        var vk_nickname = jQuery.cookie('vk_nickname');
        var vk_lastname = jQuery.cookie('vk_lastname');
        that.onLogin(vk_id, buildName(vk_firstname, vk_nickname, vk_lastname));
      }
      else {
        that.onLogout();
      }
    } else {
      that.onLogout();
    }
  }
  function authLogin(response) {
    var vk_id = response.session.mid;			jQuery.cookie('vk_id', vk_id, { expires: 365, path: '/' });
    var vk_firstname = response.session.user.first_name;	jQuery.cookie('vk_firstname', vk_firstname, { expires: 365, path: '/' });
    var vk_nickname = response.session.user.nickname;	jQuery.cookie('vk_nickname', vk_nickname, { expires: 365, path: '/' });
    var vk_lastname = response.session.user.last_name;	jQuery.cookie('vk_lastname', vk_lastname, { expires: 365, path: '/' });
    that.onLogin(vk_id, buildName(vk_firstname, vk_nickname, vk_lastname));
  }
  function authLogout(response) {
    that.onLogout();
  }
  function buildName(fn, nn, ln) {
    return fn + ' ' + nn + ' ' + ln;
  }
}
