/**
 * @file
 *
 * Form-ajax.
 */

/*
 * var socialvote_count = 2;
 * var socialvote_maxcount = 10;
 * var socialvote_options_count = count($socialvote_sections) (look at socialvote.define.inc)
 * var socialvote_options_maxcount = 10;
 */

/*
 * add/change socialvote form
 */
function socialvote_addversion() {
  jQuery('#sv_versions').append(socialvote_getinput(++socialvote_count));
  if(socialvote_count == socialvote_maxcount) {
    jQuery('#sv_addversion').css('visibility', 'hidden');
  }
}

function socialvote_delversion(num) {
  jQuery('#sv_version_wrapper' + num).remove();
  for(var i = num + 1; i <= socialvote_count; i++) {
    var value = socialvote_input_getvalue(i);
    jQuery('#sv_version_wrapper' + i).remove();
    jQuery('#sv_versions').append(socialvote_getinput(i - 1));
    socialvote_input_setvalue(i - 1, value);
  }
  jQuery('#sv_addversion').css('visibility', 'visible');
  socialvote_count--;
}

function socialvote_getinput(num) {
  return jQuery('#sv_version_wrapper-num').wrap("<div>").parent().html().replace(/-num/g, num);
}

function socialvote_input_getvalue(num) {
  var textversion = jQuery('#sv_version_wrapper' + num + ' #edit-version' + num).val();
  var countvotes = jQuery('#sv_version_wrapper' + num + ' #edit-version-votes' + num).val();
  return new socialvote_vote_class(textversion, countvotes);
}

function socialvote_input_setvalue(num, value) {
  jQuery('#sv_version_wrapper' + num + ' #edit-version' + num).val(value.textversion);
  jQuery('#sv_version_wrapper' + num + ' #edit-version-votes' + num).val(value.countvotes);
}

function socialvote_vote_class(textversion, countvotes) {
  this.textversion = textversion;
  this.countvotes = countvotes;
}

/*
 * settings form
 */
function socialvote_addsection() {
    jQuery('#sv_sections').append(socialvote_section_getinput(++socialvote_options_count));
    if(socialvote_options_count == socialvote_options_maxcount) {
      jQuery('#sv_addsection').css('visibility', 'hidden');
    }
}

function socialvote_delsection(num) {
    jQuery('#sv_section_wrapper' + num).remove();
    for(var i = num + 1; i <= socialvote_options_count; i++) {
      var value = socialvote_section_input_getvalue(i);
      jQuery('#sv_section_wrapper' + i).remove();
      jQuery('#sv_sections').append(socialvote_section_getinput(i - 1));
      socialvote_section_input_setvalue(i - 1, value);
    }
    jQuery('#sv_addsection').css('visibility', 'visible');
    socialvote_options_count--;
}

function socialvote_section_getinput(num) {
    return jQuery('#sv_section_wrapper-num').wrap("<div>").parent().html().replace(/-num/g, num);
}

function socialvote_section_input_getvalue(num) {
    //
    var text = jQuery('#sv_section_wrapper' + num + ' #edit-title' + num).val();
    var url = jQuery('#sv_section_wrapper' + num + ' #edit-url' + num).val();
    return new socialvote_section_class(text, url);
}

function socialvote_section_input_setvalue(num, value) {
    jQuery('#sv_section_wrapper' + num + ' #edit-title' + num).val(value.text);
    jQuery('#sv_section_wrapper' + num + ' #edit-url' + num).val(value.url);
}

function socialvote_section_class(text, url) {
    this.text = text;
    this.url = url;
}
