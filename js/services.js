/**
 * @file
 *
 * GET and POST to server.
 */

function socialvote_services_load(count, flag_after, flag_publish, section, svid, success, error) {
  jQuery.ajax({
    type: 'GET',
    url: '/socialvote/sv',
    data: "count=" + count + "&flag_after=" + flag_after + "&flag_publish=" + flag_publish + "&section=" + section + "&svid=" + svid,
    dataType: 'text',
    success: success,
    error: error
  });
}

function socialvote_services_vote(svid, vid, success) {
  var vote = jQuery.cookie(svid);
  if(vote) {
    //error "You was already voted"
    success('3', null, null);
    return;
  }
  else {
    jQuery.cookie(svid, vid, 365);
  }

  jQuery.ajax({
    type: 'POST',
    url: '/socialvote/sv/vote',
    data: "svid=" + svid + "&vid=" + vid + "&uid=" + user_id + "&socid=" + user_soc_id + "&uname=" + user_name,
    dataType: 'text',
    success: success,
    error: socialvote_connect_error
  });
}

function socialvote_services_generate(api_id, success) {
  jQuery.ajax({
    type: 'POST',
    url: '/socialvote/sv/generate',
    data: "api_id=" + api_id,
    dataType: 'text',
    success: success,
    error: socialvote_connect_error
  });
}

function socialvote_services_postfile(urlfrom, urlto, success) {
  jQuery.ajax({
    type: 'POST',
    url: '/socialvote/sv/postfile',
    data: 'urlfrom=' + encodeURIComponent(urlfrom) + '&urlto=' + encodeURIComponent(urlto),
    dataType: 'text',
    success: success,
    error: socialvote_connect_error
  });
}

function socialvote_connect_error(jqXHR, textStatus, errorThrown) {
  //socialvote_message_open('Error connect: ' + jqXHR.responseText);
}
