/**
 * @file
 *
 * js-messages for socialvote module.
 */

function socialvote_message_open(message) {
  jQuery('#socialvote_message_container').html(message);
  jQuery('#socialvote_opacity').css('visibility', 'visible');
  jQuery('#socialvote_message_wrapper').css('visibility', 'visible');
}

function socialvote_message_close() {
  jQuery('#socialvote_opacity').css('visibility', 'hidden');
  jQuery('#socialvote_message_wrapper').css('visibility', 'hidden');
}

function socialvote_loading_start() {
  jQuery('#socialvote_opacity').css('visibility', 'visible');
  jQuery('#socialvote_loading').css('visibility', 'visible');
}
function socialvote_loading_stop() {
  jQuery('#socialvote_opacity').css('visibility', 'hidden');
  jQuery('#socialvote_loading').css('visibility', 'hidden');
}
