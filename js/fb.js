/**
 * @file
 *
 * Contains sFB (Facebook class).
 */

function sFB(app_id, onLogin, onLogout, onInit) {
  var that = this;
  this.app_id = app_id;
  this.onLogin = onLogin;
  this.onLogout = onLogout;
  this.onInit = onInit;
  this.orig = null;
  this.init = function(onInit) {
    var onInitThis = onInit();
    window.fbAsyncInit = function() {
      that.orig = FB;
      that.orig_init();
      FB.getLoginStatus(function(response) {
        statusChange(response);
        FB.Event.subscribe('auth.statusChange', statusChange);
      });
      onInitThis();
    };
    // Load the SDK Asynchronously
    (function(d) {
      var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement('script'); js.id = id; js.async = true;
      js.src = "//connect.facebook.net/en_US/all.js";
      ref.parentNode.insertBefore(js, ref);
    }(document));
  }
  this.orig_init = function() {
    that.orig.init({
      appId      : app_id,
      status     : true,
      cookie     : true,
      xfbml      : true
    });
  }
  this.login = function() {
    FB.login(function(response) {
      if (response.authResponse) {
        FB.api('/me', function(response) {
          //login
        });
      } else {
        //cancel
      }
    });
  }
  this.logout = function() {
    FB.logout(function(response) {
      //logout
    });
  }
  this.wallpost = function(vote_text, version_text, url, img_src, success) {
    var text = sv_vote_templ;
    text = text.split('%vote%').join(vote_text);
    text = text.split('%version%').join(version_text);
    FB.ui(
      {
        method: 'feed',
        name: vote_text,
        link: url,
        picture: img_src,
        caption: sv_domain,
        description: text
      },
      function(response) {
        if (response && response.post_id) {
          success(response.post_id);
        } else {
          socialvote_loading_stop();
        }
      }
    );
  }
  this.wallpost_likeit = function(text, url, img_src, success) {
    FB.ui(
      {
        method: 'feed',
        name: text,
        link: url,
        picture: img_src,
        caption: sv_domain,
        description: text
      },
      function(response) {
        if (response && response.post_id) {
          success(response.post_id);
        } else {
          socialvote_loading_stop();
        }
      }
    );
  }
  this.comments = function(comments_selector, url) {
    jQuery(comments_selector).html('<div class="fb-comments" data-href="' + url + '" data-num-posts="2" data-width="470"></div>');
    that.orig_init();
  }
  this.error = function(error) {
    socialvote_message_open('facebook error: ' + error);
  }

  //auth.statusChange
  function statusChange(response) {
    if (response.status === 'connected') {
      FB.api('/me', function(r) {
        if(r.id !== undefined) {
          that.onLogin(r.id, r.name);
        }
      });
    } else {
      that.onLogout();
    }
  }
}
