/**
 * @file
 *
 * Contains social (wrapper for classes of social services).
 */

function social(arr, onInit) {
  var that = this;
  var count = Object.keys(arr).length;
  var soc = [];

  var init = [];
  function initlogin() {
    for(key in init) {
      soc[init[key]].onLoginWrapper();
      return;
    }
  }
  var count_onLogin = 0;
  function wrapperLogin(id) {
    count_onLogin++;
    if(count_onLogin + count_onLogout < count) {
      init.push(id);
      return false;
    }
    if(count_onLogin + count_onLogout == count) {
      init.push(id);
      initlogin();
      return false;
    }
    return true;
  }
  var count_onLogout = 0;
  function wrapperLogout(id) {
    count_onLogout++;
    if(count_onLogin + count_onLogout < count) {
      return false;
    }
    if(count_onLogin + count_onLogout == count) {
      if(count_onLogin == 0) {
        return true;
      }
      initlogin();
      return false;
    }
    return true;
  }
  var count_onInit = 0;
  function wrapperOnInit() {
    count_onInit++;
    if(count_onInit == count) {
      onInit();
    }
  }

  for(key in arr) {
    var s = new soc_wrapper(key, arr[key], wrapperLogin, wrapperLogout);
    soc[key] = s;
  }
  for(key in soc) {
    soc[key].init(wrapperOnInit);
  }

  function soc_wrapper(s_id, s, wrapperLogin, wrapperLogout, wrapperInit) {
    var that = this;
    this.s_id = s_id;
    this.app_id = s.app_id;
    this.onLogin = s.onLogin;
    this.onLogout = s.onLogout;
    this.onInit = s.onInit;
    this.orig = s.orig;
    this.init = function(onInit) { s.init(onInit); }
    s.onLogin = function(id, name) {
      that.islogin = true;
      that.id = id;
      that.name = name;
      if(wrapperLogin(s_id)) {
        that.onLogin(id, name);
      }
    }
    s.onLogout = function() {
      that.islogin = false;
      if(wrapperLogout(s_id)) {
        that.onLogout();
      }
    }

    this.wallpost = function(vote_text, version_text, url, img_src, success) { s.wallpost(vote_text, version_text, url, img_src, success); }
    this.wallpost_likeit = function(text, url, img_src, success) { s.wallpost_likeit(text, url, img_src, success); }
    this.comments = function(comments_selector, url) {
      jQuery.cookie('comments', that.s_id, { expires: 365, path: '/' });
      s.comments(comments_selector, url);
    }

    this.islogin = false;
    this.id = undefined;
    this.name = undefined;
    this.onLoginWrapper = function() {
      that.onLogin(that.id, that.name);
    }
  }

  this.getSocById = function(id) {
    return soc[id];
  }
  this.comments = function(comments_selector, url) {
    var comments = jQuery.cookie('comments');
    if(comments == null) {
      for(key in soc) {
        comments = key;
        break;
      }
    }
    soc[comments].comments(comments_selector, url);
  }
}
