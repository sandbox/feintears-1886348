/**
 * @file
 *
 * Initial social services.
 */

var onlogin_svid = null;
var onlogin_vid = null;

(function ($) { $(document).ready(function() {
  //Vkontakte
  vk = new sVK(
    vk_app_id,
    function onLogin(id, name) {
      $('#social-login').css('visibility', 'hidden');
      $('#fb').css('visibility', 'hidden');
      $('#social-logout').css('visibility', 'visible');
      $('#vk').css('visibility', 'visible');
      $('#vk .hello').text(name);
      setparams('vk', id, name);

      vote_after_login();
    },
    function onLogout() {
      $('#social-logout').css('visibility', 'hidden');
      $('#vk').css('visibility', 'hidden');
      $('#vk .hello').text('');
      $('#social-login').css('visibility', 'visible');
      clearparams();
    },
    function onInit() {}
  );

  //Facebook
  fb = new sFB(
    fb_app_id,
    function onLogin(id, name) {
      $('#social-login').css('visibility', 'hidden');
      $('#vk').css('visibility', 'hidden');
      $('#social-logout').css('visibility', 'visible');
      $('#fb').css('visibility', 'visible');
      $('#fb .hello').text(name);
      setparams('fb', id, name);

      vote_after_login();
    },
    function onLogout() {
      $('#social-logout').css('visibility', 'hidden');
      $('#fb').css('visibility', 'hidden');
      $('#fb .hello').text('');
      $('#social-login').css('visibility', 'visible');
      clearparams();
    },
    function onInit() {}
  );

  //vote if user login by click for vote
  function vote_after_login() {
    if(onlogin_svid != null && onlogin_vid != null) {
      if(onlogin_text != null && onlogin_img != null && onlogin_url != null) {
        socialvote_vote_generate2(onlogin_svid, onlogin_vid, onlogin_text, onlogin_img, onlogin_url);
      }
      else {
        socialvote_vote2(onlogin_svid, onlogin_vid);
      }
      onlogin_svid = null;
      onlogin_vid = null;
      onlogin_text = null;
      onlogin_img = null;
      onlogin_url = null;
    }
  }

  //Abstract social services
  social = new social({vk: vk, fb: fb}, function onInit() {
    social.comments('#sv_comments .sv_comments', sv_cur.uri());
  });

  function setparams(soc_id, id, name) {
    user_islogin = true;
    user_soc_id = soc_id;
    user_id = id;
    user_name = name;

    if (typeof window['socialvote_setparams'] == 'function') {
      socialvote_setparams(soc_id, id);
    }
  }
  function clearparams() {
    user_islogin = false;
    user_soc_id = '000';
    user_id = '0';
    user_name = 'Anonymous';

    if (typeof window['socialvote_clearparams'] == 'function') {
      socialvote_clearparams();
    }
  }
}); })(jQuery);
