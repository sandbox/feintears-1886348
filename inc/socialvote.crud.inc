<?php
/**
 * @file
 * Create, read, update, delete socialvote
 */

/**
 * Load current socialvote.
 */
function socialvote_load_current($flag_publish, $section_id) {
  switch ($flag_publish) {

    case SOCIALVOTE_ALL:
      $query = db_query_range('SELECT * FROM {socialvote} sv WHERE sv.section=' . $section_id . ' AND sv.api_id=0 ORDER BY svid DESC', 0, 1);
      break;

    case SOCIALVOTE_PUBLISH:
      $query = db_query_range('SELECT * FROM {socialvote} sv WHERE sv.section=' . $section_id . ' AND sv.api_id=0 AND sv.moderation=1 ORDER BY svid DESC', 0, 1);
      break;

    case SOCIALVOTE_NOTPUBLISH:
      $query = db_query_range('SELECT * FROM {socialvote} sv WHERE sv.section=' . $section_id . ' AND sv.api_id=0 AND sv.moderation=0 ORDER BY svid DESC', 0, 1);
      break;

    default:
      return FALSE;
  }
  return entity_create('socialvote', (array) current($query->fetchAllAssoc('svid')));
}

/**
 * Load.
 */
function socialvote_load($sv_id, $reset = FALSE) {
  $sv = socialvote_load_multiple(array($sv_id), array(), $reset);
  return $sv ? $sv[$sv_id] : FALSE;
}

/**
 * Load multiple.
 */
function socialvote_load_multiple($sv_ids = array(), $conditions = array(), $reset = FALSE) {
  $svs = entity_load('socialvote', $sv_ids, $conditions, $reset);
  return $svs;
}

/**
 * Save.
 */
function socialvote_save($sv) {
  entity_save('socialvote', $sv);
}

/**
 * Delete.
 */
function socialvote_delete($sv_id) {
  socialvote_delete_multiple(array($sv_id));
}

/**
 * Delete multiple.
 */
function socialvote_delete_multiple($sv_ids) {
  entity_delete_multiple('socialvote', $sv_ids);
}

/**
 * Get array of versions from string $sv->versions.
 */
function socialvote_getarrvers($str) {
  $arr1 = explode(SOCIALVOTE_VERSION_SPR2, $str);
  $arr2 = array();
  foreach ($arr1 as $item) {
    $item_arr = explode(SOCIALVOTE_VERSION_SPR1, $item);
    $arr2[] = array('text' => $item_arr[0], 'votes' => $item_arr[1]);
  }
  return $arr2;
}

/**
 * Create if not exists.
 */
function socialvote_generate($api_id, $imagefile = '') {
  if ($api_id == 0) {
    return NULL;
  }

  $arr = db_query('SELECT * FROM {socialvote} sv WHERE sv.api_id = :api_id', array(':api_id' => $api_id))->fetchAllAssoc('svid');
  if (count($arr) == 0) {
    $sv = socialvote_default();
    $sv->body = t('api_id = @api_id', array('@api_id' => $api_id));
    $sv->description = t('created by function socialvote_generate (look file socialvote/inc/socialvote.crud.inc)');
    $sv->section = 0;
    $sv->api_id = $api_id;
    $sv->imagefile = $imagefile;

    $sv->author_uid = 0;
    $sv->author_soc_id = '000';
    $sv->author_name = 'socialvote_default';
    $sv->created = REQUEST_TIME;
    $sv->changed = REQUEST_TIME;

    $sv->author_name = 'socialvote_generate';
    socialvote_save($sv);
  }
  else {
    $sv = entity_create('socialvote', (array) current($arr));
  }
  return $sv;
}

/**
 * Returns portion of votes.
 *
 * @param int $flag_after
 *   The flag for load. 1 - after sv-id incl., 0 - before sv-id incl.
 *
 * @param int $flag_publish
 *   The flag for identify type of votes:
 *   - SOCIALVOTE_ALL: all votes
 *   - SOCIALVOTE_PUBLISH: only publish votes
 *   - SOCIALVOTE_NOTPUBLISH: only no publish votes
 *
 * @param int $svid
 *   First sv-id of portion to load.
 *
 * @param int $count
 *   Count of votes in portion.
 *
 * @return array
 *   An array of votes
 */
function socialvote_load_multiple_portion($flag_after, $flag_publish, $section, $svid, $count = SOCIALVOTE_LOADPORTION) {
  $where = ($flag_after == 1) ? "sv.svid >= $svid" : "sv.svid <= $svid order by sv.svid desc";
  switch ($flag_publish) {
    case SOCIALVOTE_ALL:
      $query = db_query_range('SELECT * FROM {socialvote} sv WHERE sv.section=' . $section . ' AND sv.api_id=0 AND ' . $where, 0, $count);
      break;

    case SOCIALVOTE_PUBLISH:
      $query = db_query_range('SELECT * FROM {socialvote} sv WHERE sv.section=' . $section . ' AND sv.api_id=0 AND sv.moderation=1 AND ' . $where, 0, $count);
      break;

    case SOCIALVOTE_NOTPUBLISH:
      $query = db_query_range('SELECT * FROM {socialvote} sv WHERE sv.section=' . $section . ' AND sv.api_id=0 AND sv.moderation=0 AND ' . $where, 0, $count);
      break;

    default:
      return FALSE;
  }
  return $query->fetchAllAssoc('svid');
}

/**
 * Function for vote.
 *
 * @param int $sv_id
 *   id of vote
 *
 * @param int $vid
 *   id of version
 *
 * @param int $user_id
 *   id of user
 *
 * @param int $soc_id
 *   'fb' - facebook, 'vk' - vkontakte, '000' - core
 *
 * @param string $user_name
 *   Name of user
 *
 * @return int
 *   Flag of success (look at inc/socialvote.define.inc)
 *     SOCIALVOTE_VOTE_RESPONSE_OK     - succesfully vote
 *     SOCIALVOTE_VOTE_RESPONSE_ERROR1 - incorrect id of vote or version
 *     SOCIALVOTE_VOTE_RESPONSE_ERROR2 - error of social authorization
 *     SOCIALVOTE_VOTE_RESPONSE_ERROR3 - this user already vote
 */
function socialvote_vote($sv_id, $vid, $user_id, $soc_id, $user_name) {

  // Load sv.
  $sv = socialvote_load($sv_id);
  if (!$sv) {
    // Incorrect id of vote or version.
    return SOCIALVOTE_VOTE_RESPONSE_ERROR1;
  }

  $versions = explode(SOCIALVOTE_VERSION_SPR2, $sv->versions);
  if ($vid >= count($versions)) {
    // Incorrect id of vote or version.
    return SOCIALVOTE_VOTE_RESPONSE_ERROR1;
  }

  // Identity of the user.
  switch ($soc_id) {
    case SOCIALVOTE_S_VK:
      $member = socialvote_vk_authOpenAPIMember();
      if ($member === FALSE) {
        // Error of social authorization.
        return SOCIALVOTE_VOTE_RESPONSE_ERROR2;
      }
      if ($member['id'] != $user_id) {
        // Error of social authorization.
        return SOCIALVOTE_VOTE_RESPONSE_ERROR2;
      }
      break;

    case SOCIALVOTE_S_FB:
      $user_true_id = socialvote_fb_getuserid();
      if (!$user_true_id) {
        // Error of social authorization.
        return SOCIALVOTE_VOTE_RESPONSE_ERROR2;
      }
      if ($user_true_id != $user_id) {
        // Error of social authorization.
        return SOCIALVOTE_VOTE_RESPONSE_ERROR2;
      }
      break;

    default:
      // Error of social authorization.
      return SOCIALVOTE_VOTE_RESPONSE_ERROR2;
  }

  // Check record in log.
  if (socialvote_checkinlog($sv_id, $user_id, $soc_id)) {
    // This user already vote.
    return SOCIALVOTE_VOTE_RESPONSE_ERROR3;
  }

  // Add +1 to version.
  $version = explode(SOCIALVOTE_VERSION_SPR1, $versions[$vid]);
  $version[1]++;

  $versions[$vid] = $version[0] . SOCIALVOTE_VERSION_SPR1 . $version[1];
  $sv->versions = implode(SOCIALVOTE_VERSION_SPR2, $versions);

  // Save sv.
  $sv->save();

  // Hooks and rules.
  if (module_exists('rules') && $sv->api_id == 0) {
    rules_invoke_event('socialvote_rules_vote', $user_name, socialvote_user_uri($soc_id, $user_id), socialvote_url($sv), $version[0]);
  }
  module_invoke_all('socialvote_vote', $sv->svid, $vid, $version[0], $version[1], $user_id, $soc_id, $user_name);

  return SOCIALVOTE_VOTE_RESPONSE_OK;
}

/**
 * Function authOpenAPIMember from vk-documentation.
 *
 * http://vk.com/developers.php?oid=-1&p=VK.Auth
 */
function socialvote_vk_authOpenAPIMember() {
  $session = array();
  $member = FALSE;
  $valid_keys = array('expire', 'mid', 'secret', 'sid', 'sig');
  $app_cookie = $_COOKIE['vk_app_' . SOCIALVOTE_VK_APPID];
  if ($app_cookie) {
    $session_data = explode('&', $app_cookie, 10);
    foreach ($session_data as $pair) {
      list($key, $value) = explode('=', $pair, 2);
      if (empty($key) || empty($value) || !in_array($key, $valid_keys)) {
        continue;
      }
      $session[$key] = $value;
    }
    foreach ($valid_keys as $key) {
      if (!isset($session[$key])) {
        return $member;
      }
    }
    ksort($session);

    $sign = '';
    foreach ($session as $key => $value) {
      if ($key != 'sig') {
        $sign .= ($key . '=' . $value);
      }
    }
    $sign .= SOCIALVOTE_VK_SECRET;
    $sign = md5($sign);
    if ($session['sig'] == $sign && $session['expire'] > time()) {
      $member = array(
        'id' => intval($session['mid']),
        'secret' => $session['secret'],
        'sid' => $session['sid'],
      );
    }
  }
  return $member;
}

/**
 * Code from fb-documentation.
 *
 * http://developers.facebook.com/docs/guides/web/#login
 */
function socialvote_fb_getuserid() {
  if (($library = libraries_load('facebook-php-sdk')) && !empty($library['loaded'])) {
    $facebook = new Facebook(array(
      'appId'  => SOCIALVOTE_FB_APPID,
      'secret' => SOCIALVOTE_FB_SECRET,
    ));

    return $facebook->getUser();
  }
  else {
    watchdog('socialvote', t('Unable to load the required Facebook library.'));
    return null;
  }
}

/**
 * Check record in log.
 *
 * @param int $sv_id
 *   id of vote
 *
 * @param int $user_id
 *   Id of user
 *
 * @param string $soc_id
 *   'fb' - facebook, 'vk' - vkontakte, '000' - core
 *
 * @param int $careful
 *   Check during period in seconds
 *
 * @return bool
 *   Flag of successfully
 */
function socialvote_checkinlog($sv_id, $user_id, $soc_id, $careful = SOCIALVOTE_CAREFUL_INSPECTION) {
  $query = db_query_range('SELECT svlid FROM {socialvote_log} WHERE 1 ORDER BY timestamp DESC', 0, $careful);
  $ids = array();
  while ($record = $query->fetchAssoc()) {
    $ids[] = $record['svlid'];
  }

  if (empty($ids)) {
    return FALSE;
  }

  $query = db_query('SELECT svlid FROM {socialvote_log} WHERE svid = :sv_id AND user_id = :user_id AND user_soc_id = :soc_id AND svlid in (' . implode($ids, ',') . ')',
    array(
      ':sv_id' => $sv_id,
      ':user_id' => $user_id, ':soc_id' => $soc_id,
    )
  )->fetchAssoc();
  return (!empty($query));
}
