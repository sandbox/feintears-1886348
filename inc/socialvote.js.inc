<?php
/**
 * @file
 * PHP -> JavaScript.
 */

/**
 * Implements hook_js_alter().
 */
function socialvote_js_alter(&$js) {
  $code = <<<EOT
    //socialvote module
    var vk_app_id = 'SOCIALVOTE_VK_APPID';
    var fb_app_id = 'SOCIALVOTE_FB_APPID';
    var social;//array of social services like vk or fb
    var user_islogin = false, user_soc_id = '000', user_id = '0', user_name = 'Anonymous';//default values
EOT;
  $code = str_replace('SOCIALVOTE_VK_APPID', SOCIALVOTE_VK_APPID, $code);
  $code = str_replace('SOCIALVOTE_FB_APPID', SOCIALVOTE_FB_APPID, $code);

  $code_arr = drupal_js_defaults($code);
  $code_arr['type'] = 'inline';

  $js['socialvote'] = $code_arr;

  drupal_add_library('system', 'jquery.cookie');
}
