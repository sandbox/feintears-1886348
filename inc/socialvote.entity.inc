<?php
/**
 * @file
 * Entity
 */

/**
 * Implements hook_entity_info().
 */
function socialvote_entity_info() {
  return array(
    'socialvote' => array(
      'label' => t('Social vote'),
      'plural label' => t('Social votes'),
      'description' => t('Votes by using social accounts'),
      'base table' => 'socialvote',
      'entity class' => 'Entity',
      'controller class' => 'EntityAPIController',
      'fieldable' => FALSE,
      'entity keys' => array(
        'id' => 'svid',
      ),
      'admin ui' => array(
        'path' => 'admin/structure/socialvotes',
        'controller class' => 'SocialVoteUIController',
      ),
      'view modes' => array(
        'inpage' => array(
          'label' => t('In Page'),
        ),
        'inblock' => array(
          'label' => t('In Block'),
        ),
      ),
      'access callback' => 'socialvote_access',
      'load hook' => 'socialvote_load',
      'label callback' => 'socialvote_label',
      'uri callback' => 'socialvote_uri',
      'module' => 'socialvote',
      'exportable' => TRUE,
    ),
  );
}

/**
 * Access callback.
 */
function socialvote_access($op, $entity, $account = NULL, $entity_type = 'socialvote') {
  return user_access('administer site configuration');
}

/**
 * Label callback.
 */
function socialvote_label($sv) {
  return isset($sv->body) ? $sv->body : NULL;
}

/**
 * URL callback.
 */
function socialvote_url($sv) {
  $url_arr = socialvote_uri($sv);
  return $GLOBALS['base_root'] . '/' . $url_arr['path'];
}

/**
 * URI callback.
 */
function socialvote_uri($sv) {
  global $_socialvote_sections;
  if ($sv->section == 0) {
    return array();
  }
  $section_url = $_socialvote_sections[$sv->section]['url'];
  if (defined('SOCIALVOTE_PAGELINK_PUBLISH') && $sv->moderation == 1) {
    return array('path' => SOCIALVOTE_PAGELINK_PUBLISH . '/' . $section_url . '/' . $sv->svid);
  }
  if (defined('SOCIALVOTE_PAGELINK_NOTPUBLISH') && $sv->moderation == 0) {
    return array('path' => SOCIALVOTE_PAGELINK_NOTPUBLISH . '/' . $section_url . '/' . $sv->svid);
  }
  if (defined('SOCIALVOTE_PAGELINK_ALL')) {
    return array('path' => SOCIALVOTE_PAGELINK_ALL . '/' . $section_url . '/' . $sv->svid);
  }
  return array();
}

/**
 * URI of user from social service.
 */
function socialvote_user_uri($user_soc_id, $user_id) {
  switch ($user_soc_id) {
    case SOCIALVOTE_S_VK:
      return 'http://vk.com/id' . $user_id;

    case SOCIALVOTE_S_FB:
      return 'http://facebook.com/profile.php?id=' . $user_id;

    default:
      return '#';
  }
}

/**
 * Entity don't work without it.
 */
class SocialVoteAPIController extends EntityAPIController {


}

/**
 * UI for page /admin/structure/socialvotes.
 */
class SocialVoteUIController extends EntityDefaultUIController {
  /**
   * Overview header.
   */
  protected function overviewTableHeaders($conditions, $rows, $additional_header = array()) {
    $header = parent::overviewTableHeaders($conditions, $rows, $additional_header);
    $header['0'] = t('Social vote');
    return $header;
  }

  /**
   * Overview row.
   */
  protected function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    $row = parent::overviewTableRow($conditions, $id, $entity, $additional_cols);
    $row[0]['data']['#theme'] = 'socialvote_ui_overview_item';
    return $row;
  }
}

/**
 * Empty socialvote class by default.
 */
function socialvote_default() {
  $sv = entity_create('socialvote', array());

  $sv->is_new = TRUE;
  $sv->body = '';
  $sv->description = '';

  $vers1 = 'yes' . SOCIALVOTE_VERSION_SPR1 . '0';
  $vers2 = 'no' . SOCIALVOTE_VERSION_SPR1 . '0';
  $sv->versions = $vers1 . SOCIALVOTE_VERSION_SPR2 . $vers2;

  $sv->versions_count = 2;

  return $sv;
}
