<?php
/**
 * @file
 * Services
 */

/**
 * Implements hook_services_resources().
 */
function socialvote_services_resources() {
  return array(
    'sv' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'socialvote',
          'name' => 'inc/socialvote.services',
        ),
        'callback' => 'socialvote_services_load',
        'args' => array(
          array(
            'name' => 'count',
            'optional' => FALSE,
            'type' => 'int',
            'description' => 'count for loading. 0 - value by default',
            'default value' => 0,
            'source' => array('param' => 'count'),
          ),
          array(
            'name' => 'flag_after',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'flag for load. 1 - after sv-id incl., 0 - before sv-id incl.',
            'default value' => 1,
            'source' => array('param' => 'flag_after'),
          ),
          array(
            'name' => 'flag_publish',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'flag for load. SOCIALVOTE_ALL - all votes, SOCIALVOTE_PUBLISH - publish votes, SOCIALVOTE_NOTPUBLISH - not publish votes.',
            'default value' => SOCIALVOTE_ALL,
            'source' => array('param' => 'flag_publish'),
          ),
          array(
            'name' => 'section',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'section id',
            'default value' => 0,
            'source' => array('param' => 'section'),
          ),
          array(
            'name' => 'svid',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'id of vote to load',
            'default value' => 1,
            'source' => array('param' => 'svid'),
          ),
        ),
        'access arguments' => array('access content'),
      ),
      'actions' => array(
        'vote' => array(
          'file' => array(
            'type' => 'inc',
            'module' => 'socialvote',
            'name' => 'inc/socialvote.services',
          ),
          'callback' => 'socialvote_services_vote',
          'help' => t('This method allows to vote.'),
          'args' => array(
            array(
              'name' => 'svid',
              'optional' => FALSE,
              'type' => 'int',
              'description' => 'id of socialvote to vote',
              'source' => array('data' => 'svid'),
            ),
            array(
              'name' => 'vid',
              'optional' => FALSE,
              'type' => 'int',
              'description' => 'id of version to load',
              'source' => array('data' => 'vid'),
            ),
            array(
              'name' => 'uid',
              'optional' => FALSE,
              'type' => 'int',
              'description' => 'id of voter',
              'source' => array('data' => 'uid'),
            ),
            array(
              'name' => 'socid',
              'optional' => FALSE,
              'type' => 'string',
              'description' => 'soc id of voter (000 - core, fb - facebook, vk - vkontakte)',
              'source' => array('data' => 'socid'),
            ),
            array(
              'name' => 'uname',
              'optional' => FALSE,
              'type' => 'string',
              'description' => 'soc id of voter (000 - core, fb - facebook, vk - vkontakte)',
              'source' => array('data' => 'uname'),
            ),
          ),
          'access arguments' => array('access content'),
        ),
        'generate' => array(
          'file' => array(
            'type' => 'inc',
            'module' => 'socialvote',
            'name' => 'inc/socialvote.services',
          ),
          'callback' => 'socialvote_services_generate',
          'help' => t('This method generate a new socialvote by api_id if not exists.'),
          'args' => array(
            array(
              'name' => 'api_id',
              'optional' => FALSE,
              'type' => 'int',
              'description' => 'api_id of socialvote',
              'source' => array('data' => 'api_id'),
            ),
          ),
          'access arguments' => array('access content'),
        ),
        'postfile' => array(
          'file' => array(
            'type' => 'inc',
            'module' => 'socialvote',
            'name' => 'inc/socialvote.services',
          ),
          'callback' => 'socialvote_services_postfile',
          'help' => t('drupal_http_request file (need it for vk.com api post image)'),
          'args' => array(
            array(
              'name' => 'urlfrom',
              'optional' => FALSE,
              'type' => 'string',
              'description' => 'url of file for post',
              'source' => array('data' => 'urlfrom'),
            ),
            array(
              'name' => 'urlto',
              'optional' => FALSE,
              'type' => 'string',
              'description' => 'servers\' url',
              'source' => array('data' => 'urlto'),
            ),
          ),
          'access arguments' => array('access content'),
        ),
      ),
    ),
  );
}

/**
 * Index callback.
 *
 * GET socialvote/sv.
 */
function socialvote_services_load($count, $flag_after, $flag_publish, $section, $svid) {
  if ($count == 0) {
    $count = SOCIALVOTE_LOADPORTION;
  }

  // +1 for return $arr['more'].
  $arr = socialvote_load_multiple_portion($flag_after, $flag_publish, $section, $svid, $count + 2);

  if (count($arr) == $count + 2) {
    end($arr);
    unset($arr[key($arr)]);
    $arr['more'] = 'yes';
  }
  else {
    $arr['more'] = 'no';
  }

  return $arr;
}

/**
 * Action 'vote' callback.
 *
 * POST /socialvote/sv/vote + body data.
 */
function socialvote_services_vote($svid, $vid, $uid, $socid, $uname) {
  return socialvote_vote($svid, $vid, $uid, $socid, $uname);
}

/**
 * Action 'generate' callback.
 *
 * POST /socialvote/sv/generate + body data.
 */
function socialvote_services_generate($api_id) {
  $sv = socialvote_generate($api_id);
  $versions = explode(SOCIALVOTE_VERSION_SPR2, $sv->versions);
  $vers1 = explode(SOCIALVOTE_VERSION_SPR1, $versions[0]);
  $vers2 = explode(SOCIALVOTE_VERSION_SPR1, $versions[1]);
  return array($sv->svid, array($vers1[1], $vers2[1]));
}

/**
 * Action 'postfile' callback.
 *
 * POST file from $urlfrom to server $urlto.
 */
function socialvote_services_postfile($urlfrom, $urlto) {
  return array(
    'response' => _socialvote_request(
      $urlto,
      array(),
      array('photo' => $urlfrom),
      'POST'
    ),
  );
}

/**
 * HTTP-request.
 */
function _socialvote_request($request_url, $params = array(), $files = array(), $method = 'GET') {
  $boundary = 'A0sFSD';
  $headers = array("Content-Type" => "multipart/form-data; boundary=$boundary");
  $request = drupal_http_request($request_url, array(
      'headers' => $headers,
      'method' => $method,
      'data' => _socialvote_multipart_encode($boundary, $params, $files),
    )
  );

  if ($request->error) {
    watchdog("socialvote", "Request failed - " . $request->error . ' - ' . http_build_query($params));
  }

  return $request->data;
}

/**
 * Encode params and files.
 */
function _socialvote_multipart_encode($boundary, $params, $files) {
  $output = "";

  foreach ($params as $key => $value) {
    $output .= "--$boundary\r\n";
    $output .= _socialvote_multipart_enc_text($key, $value);
  }

  foreach ($files as $key => $value) {
    $output .= "--$boundary\r\n";
    $output .= _socialvote_multipart_enc_file($key, $value);
  }

  $output .= "--$boundary--";

  return $output;
}

/**
 * Encode text.
 */
function _socialvote_multipart_enc_text($name, $value) {
  return "Content-Disposition: form-data; name=\"$name\"\r\n\r\n$value\r\n";
}

/**
 * Encode file.
 */
function _socialvote_multipart_enc_file($name, $path) {
  if (substr($path, 0, 1) == "@") {
    $path = substr($path, 1);
  }
  $filename = basename($path);
  $mimetype = "application/octet-stream";
  $data = "Content-Disposition: form-data; name=\"$name\"; filename=\"$filename\"\r\n";
  $data .= "Content-Transfer-Encoding: binary\r\n";
  $data .= "Content-Type: $mimetype\r\n\r\n";
  $data .= file_get_contents($path) . "\r\n";

  return $data;
}
