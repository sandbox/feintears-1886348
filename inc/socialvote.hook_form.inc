<?php
/**
 * @file
 * Add/change socialvote form.
 */

/**
 * Implements hook_form().
 */
function socialvote_form($form, &$form_state, $sv = NULL) {
  if (is_null($sv)) {
    $sv = socialvote_default();
  }

  global $user;
  $is_admin = user_access('administer socialvotes');
  $is_new = isset($sv->is_new);
  $versions = $is_new ? NULL : socialvote_getarrvers($sv->versions);
  $versions_count = $is_new ? 2 : count($versions);

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'socialvote') . '/css/form.css',
  );
  $form['#attached']['js'] = array(
    'var socialvote_count = ' . $versions_count . ', socialvote_maxcount = ' . SOCIALVOTE_MAXVERSIONS . ';' => array(
      'type' => 'inline',
    ),
    drupal_get_path('module', 'socialvote') . '/js/form.js' => array(
      'type' => 'file',
    ),
  );

  $options = array();
  foreach ($GLOBALS['_socialvote_sections'] as $key => $sect) {
    $options[$key] = $sect['name'];
  }
  $form['section'] = array(
    '#title' => t('Section'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 1,
    '#required' => TRUE,
  );
  $form['body'] = array(
    '#title' => t('Poll'),
    '#type' => 'textfield',
    '#default_value' => $is_new ? '' : $sv->body,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#rows' => '5',
    '#cols' => '60',
    '#default_value' => $is_new ? '' : $sv->description,
  );
  $form['imagefile'] = array(
    '#title' => t('Image'),
    '#type' => 'managed_file',
    '#size' => 22,
    '#upload_validators' => array(
      'file_validate_extensions' => array('jpg jpeg gif png', 'txt'),
      'file_validate_size' => array(1 * 1024 * 1024),
    ),
    '#upload_location' => 'public://sv/images',
  );
  if (!$is_new && $is_admin) {
    $form['image'] = array(
      '#markup' => '<div>' . theme_image(
        array(
          'path' => $sv->imagefile,
          'alt' => $sv->description,
          'title' => $sv->description,
          'attributes' => array(),
        )
      ) . '</div>',
    );
  }

  $form['versions'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'sv_versions'),
  );

  // Invisible template for js.
  socialvote_version_form($form, '-num', TRUE, $is_admin, NULL);
  // 1.
  socialvote_version_form($form, 1, $is_new, $is_admin, $versions[0]);
  // 2.
  socialvote_version_form($form, 2, $is_new, $is_admin, $versions[1]);

  if (!$is_new) {
    // Other.
    for ($i = 2; $i < $versions_count; $i++) {
      socialvote_version_form($form, $i + 1, $is_new, $is_admin, $versions[$i]);
    }
  }

  $img = theme_image(
    array(
      'path' => drupal_get_path('module', 'socialvote') . '/images/add.png',
      'width' => 25,
      'height' => 25,
      'alt' => t('add'),
      'title' => t('add'),
      'attributes' => array(),
    )
  );
  $form['addversion'] = array(
    '#markup' => '<div id="sv_addversion"><a href="#sv_versions" onclick="socialvote_addversion()">' . $img . '</a></div>',
  );

  if ($is_admin) {
    $form['moderation'] = array(
      '#title' => t('Publish'),
      '#type' => 'checkbox',
      '#default_value' => $is_new ? 0 : $sv->moderation,
      '#return_value' => 1,
    );
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Element of dynamic part main form.
 */
function socialvote_version_form(&$form, $num, $is_new, $is_admin, $vers = NULL) {

  // First two versions is require.
  $require = ($num == 1 || $num == 2);

  $form['versions']['version_wrapper' . $num] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'sv_version_wrapper' . $num,
      'class' => array('sv_version_wrapper'),
    ),
  );
  $form['versions']['version_wrapper' . $num]['version' . $num] = array(
    '#title' => t('Version @num', array('@num' => $num)),
    '#type' => 'textfield',
    '#required' => $require,
    '#default_value' => $is_new ? '' : $vers['text'],
    '#theme_wrappers' => array('socialvote_field_version'),
  );
  if ($is_admin) {
    $form['versions']['version_wrapper' . $num]['version_votes' . $num] = array(
      '#title' => t('Count of votes'),
      '#type' => 'textfield',
      '#base_type' => 'number',
      '#size' => 4,
      '#attributes' => array('class' => array('sv_countversion')),
      '#default_value' => $is_new ? '0' : $vers['votes'],
      '#theme_wrappers' => array('socialvote_field_version'),
      '#element_validate' => array('element_validate_number'),
    );
  }
  if (!$require) {
    $img = theme_image(
      array(
        'path' => drupal_get_path('module', 'socialvote') . '/images/del.png',
        'width' => 15,
        'height' => 15,
        'alt' => t('delete'),
        'title' => t('delete'),
        'attributes' => array(),
      )
    );
    $form['versions']['version_wrapper' . $num]['delversion' . $num] = array(
      '#markup' => '<div class="href"><a href="#sv_versions" onclick="socialvote_delversion(' . $num . ')">' . $img . '</a></div>',
    );
  }
}

/**
 * Implements hook_form_submit().
 */
function socialvote_form_submit(&$form, &$form_state) {
  global $user;
  $is_admin = user_access('administer socialvotes');
  $input = $form_state['input'];
  if (!isset($form_state['entity_type'])) {
    $form_state['entity_type'] = 'socialvote';
  }
  if (!isset($form_state['socialvote'])) {
    $form_state['socialvote'] = socialvote_default();
  }
  $sv = entity_ui_form_submit_build_entity($form, $form_state);
  $is_new = isset($sv->is_new) || !isset($sv->svid);
  $sv->body = drupal_html_to_text($input['body']);
  $sv->description = drupal_html_to_text($input['description']);

  // Two require fields.
  $versions = array(
    socialvote_version_form_submit($input, 1, $is_admin),
    socialvote_version_form_submit($input, 2, $is_admin),
  );

  for ($i = 3; TRUE; $i++) {
    // Added.
    if (isset($input['version' . $i])) {
      $versions[] = socialvote_version_form_submit($input, $i, $is_admin);
    }
    else {
      break;
    }
  }
  $sv->versions = implode(SOCIALVOTE_VERSION_SPR2, $versions);
  $sv->versions_count = count($versions);

  $fileid = $form_state['values']['imagefile'];
  if ($fileid == 0) {
    if ($is_new) {
      $d = file_scan_directory(drupal_get_path('module', 'socialvote') . '/images/default', '/.*/');
      $d = array_values($d);
      $r = rand(0, count($d) - 1);
      $sv->imagefile = $d[$r]->uri;
    }
    else {
      $sv_old = socialvote_load($sv->svid);
      $sv->imagefile = $sv_old->imagefile;
    }
  }
  else {
    $file = file_load($fileid);
    var_dump($file);
    $file->status = FILE_STATUS_PERMANENT;
    $sv->imagefile = $file->uri;
    file_save($file);
  }

  // Temporary block.
  $sv->author_uid = $user->uid;
  $sv->author_soc_id = SOCIALVOTE_S_CORE;
  $sv->author_name = isset($user->name) ? $user->name : 'Anonymous';

  $sv->section = $input['section'];
  $sv->moderation = ($is_admin) ? $input['moderation'] : 0;
  $sv->api = 0;
  if ($is_new) {
    $sv->created = REQUEST_TIME;
  }
  $sv->changed = REQUEST_TIME;

  socialvote_save($sv);

  if (module_exists('rules')) {
    rules_invoke_event('socialvote_rules_create_sv', $sv->body, $sv->description, socialvote_url($sv));
  }

  $form_state['redirect'] = socialvote_url($sv);
}

/**
 * Version part submit.
 */
function socialvote_version_form_submit($input, $num, $is_admin) {
  if ($is_admin) {
    $countvotes = isset($input['version_votes' . $num]) ? $input['version_votes' . $num] : 0;
    return $input['version' . $num] . SOCIALVOTE_VERSION_SPR1 . (is_numeric($countvotes) ? $countvotes : 0);
  }
  else {
    return $input['version' . $num] . SOCIALVOTE_VERSION_SPR1 . 0;
  }
}

/**
 * Options form.
 */

/**
 * Implements hook_form().
 */
function socialvote_options_form($form, &$form_state, $sv = NULL) {
  global $_socialvote_sections_en;

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'socialvote') . '/css/form.css' => array(
      'type' => 'file',
    ),
  );
  $form['#attached']['js'] = array(
    'var socialvote_options_count = ' . count($socialvote_sections_en) . ', socialvote_options_maxcount = ' . SOCIALVOTE_MAXVERSIONS . ';' => array(
      'type' => 'inline',
    ),
    drupal_get_path('module', 'socialvote') . '/js/form.js' => array(
      'type' => 'file',
    ),
  );

  // Ids.
  $form['vk_appid'] = array(
    '#title' => t('Vk application ID'),
    '#type' => 'textfield',
    '#default_value' => SOCIALVOTE_VK_APPID,
    '#required' => TRUE,
  );
  $form['vk_secret'] = array(
    '#title' => t('Vk application SECRET'),
    '#type' => 'textfield',
    '#default_value' => SOCIALVOTE_VK_SECRET,
    '#required' => TRUE,
  );
  $form['fb_appid'] = array(
    '#title' => t('Facebook application ID'),
    '#type' => 'textfield',
    '#default_value' => SOCIALVOTE_FB_APPID,
    '#required' => TRUE,
  );
  $form['fb_secret'] = array(
    '#title' => t('Facebook application SECRET'),
    '#type' => 'textfield',
    '#default_value' => SOCIALVOTE_FB_SECRET,
    '#required' => TRUE,
  );

  // Pages urls and titles.
  $form['pagelink_addvote'] = array(
    '#title' => t('URL of page for add new vote (without /)'),
    '#type' => 'textfield',
    '#default_value' => SOCIALVOTE_PAGELINK_ADDVOTE,
    '#required' => TRUE,
  );
  $form['pagetitle_addvote'] = array(
    '#title' => t('Title of this page (translatable string)'),
    '#type' => 'textfield',
    '#default_value' => $GLOBALS['SOCIALVOTE_PAGETITLE_ADDVOTE'],
    '#required' => TRUE,
  );
  $form['pagelink_all'] = array(
    '#title' => t('URL of page for all socialvotes (enter empty string for shut off this page)'),
    '#type' => 'textfield',
    '#default_value' => defined('SOCIALVOTE_PAGELINK_ALL') ? SOCIALVOTE_PAGELINK_ALL : '',
    '#required' => FALSE,
  );
  $form['pagetitle_all'] = array(
    '#title' => t('Title of this page (translatable string)'),
    '#type' => 'textfield',
    '#default_value' => defined('SOCIALVOTE_PAGELINK_ALL') ? $GLOBALS['SOCIALVOTE_PAGETITLE_ALL'] : '',
    '#required' => FALSE,
  );
  $form['pagelink_publish'] = array(
    '#title' => t('URL of page for publish socialvotes (enter empty string for shut off this page)'),
    '#type' => 'textfield',
    '#default_value' => defined('SOCIALVOTE_PAGELINK_PUBLISH') ? SOCIALVOTE_PAGELINK_PUBLISH : '',
    '#required' => FALSE,
  );
  $form['pagetitle_publish'] = array(
    '#title' => t('Title of this page (translatable string)'),
    '#type' => 'textfield',
    '#default_value' => defined('SOCIALVOTE_PAGELINK_PUBLISH') ? $GLOBALS['SOCIALVOTE_PAGETITLE_PUBLISH'] : '',
    '#required' => FALSE,
  );
  $form['pagelink_notpublish'] = array(
    '#title' => t('URL of page for not publish socialvotes (enter empty string for shut off this page)'),
    '#type' => 'textfield',
    '#default_value' => defined('SOCIALVOTE_PAGELINK_NOTPUBLISH') ? SOCIALVOTE_PAGELINK_NOTPUBLISH : '',
    '#required' => FALSE,
  );
  $form['pagetitle_notpublish'] = array(
    '#title' => t('Title of this page (translatable string)'),
    '#type' => 'textfield',
    '#default_value' => defined('SOCIALVOTE_PAGELINK_NOTPUBLISH') ? $GLOBALS['SOCIALVOTE_PAGETITLE_NOTPUBLISH'] : '',
    '#required' => FALSE,
  );

  // Sections (submenu of 'publish', 'not-publish' and 'all' items).
  $form['sections'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'sv_sections'),
  );
  $form['sections']['markup'] = array(
    '#markup' => "<div class='markup'>" . t("Sections (submenu of 'publish', 'not-publish' and 'all' items)") . ': </div>',
  );
  socialvote_options_section_form($form, '-num');
  foreach ($_socialvote_sections_en as $key => $value) {
    socialvote_options_section_form($form, $key, t($value['name'], array(), array('langcode' => 'en')), $value['url']);
  }
  $img = theme_image(
    array(
      'path' => drupal_get_path('module', 'socialvote') . '/images/add.png',
      'width' => 25,
      'height' => 25,
      'alt' => t('add'),
      'title' => t('add'),
      'attributes' => array(),
    )
  );
  $form['addsection'] = array(
    '#markup' => '<div id="sv_addsection"><a href="#sv_sections" onclick="socialvote_addsection()">' . $img . '</a></div>',
  );

  $form['section_x'] = array(
    '#title' => t('Count of socialvotes in one row of section'),
    '#type' => 'textfield',
    '#base_type' => 'number',
    '#size' => 4,
    '#attributes' => array('class' => array('sv_sectionsize')),
    '#default_value' => SOCIALVOTE_SECTION_X,
    '#theme_wrappers' => array('socialvote_options_field_section'),
    '#element_validate' => array('element_validate_number'),
  );

  $form['section_y'] = array(
    '#title' => t('Count of rows in one page'),
    '#type' => 'textfield',
    '#base_type' => 'number',
    '#size' => 4,
    '#attributes' => array('class' => array('sv_sectionsize')),
    '#default_value' => SOCIALVOTE_SECTION_Y,
    '#theme_wrappers' => array('socialvote_options_field_section'),
    '#element_validate' => array('element_validate_number'),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Element of dynamic part settings form.
 */
function socialvote_options_section_form(&$form, $num, $def_title = '', $def_url = '') {
  $form['sections']['section_wrapper' . $num] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => 'sv_section_wrapper' . $num,
      'class' => array('sv_section_wrapper'),
    ),
  );
  $form['sections']['section_wrapper' . $num]['title' . $num] = array(
    '#title' => t('Title @num', array('@num' => $num)),
    '#type' => 'textfield',
    '#required' => FALSE,
    '#default_value' => $def_title,
    '#size' => 20,
    '#attributes' => array('class' => array('sv_section_title')),
  );
  $form['sections']['section_wrapper' . $num]['url' . $num] = array(
    '#title' => t('Url @num', array('@num' => $num)),
    '#type' => 'textfield',
    '#required' => FALSE,
    '#default_value' => $def_url,
    '#size' => 20,
    '#attributes' => array('class' => array('sv_section_url')),
  );
  if ($num > 1 || $num == '-num') {
    $img = theme_image(
      array(
        'path' => drupal_get_path('module', 'socialvote') . '/images/del.png',
        'width' => 15, 'height' => 15,
        'alt' => t('delete'),
        'title' => t('delete'),
        'attributes' => array(),
      )
    );
    $form['sections']['section_wrapper' . $num]['delsection' . $num] = array(
      '#markup' => '<div class="href"><a href="#sv_sections" onclick="socialvote_delsection(' . $num . ')">' . $img . '</a></div>',
    );
  }
}


/**
 * Implements hook_form_submit().
 */
function socialvote_options_form_submit(&$form, &$form_state) {
  $input = $form_state['input'];
  variable_set('socialvote_vk_appid', $input['vk_appid']);
  variable_set('socialvote_vk_secret', $input['vk_secret']);
  variable_set('socialvote_fb_appid', $input['fb_appid']);
  variable_set('socialvote_fb_secret', $input['fb_secret']);

  variable_set('socialvote_pagelink_addvote', $input['pagelink_addvote']);
  variable_set('socialvote_pagetitle_addvote', $input['pagetitle_addvote']);
  variable_set('socialvote_pagelink_all', $input['pagelink_all']);
  variable_set('socialvote_pagetitle_all', $input['pagetitle_all']);
  variable_set('socialvote_pagelink_publish', $input['pagelink_publish']);
  variable_set('socialvote_pagetitle_publish', $input['pagetitle_publish']);
  variable_set('socialvote_pagelink_notpublish', $input['pagelink_notpublish']);
  variable_set('socialvote_pagetitle_notpublish', $input['pagetitle_notpublish']);

  $sections = array();
  for ($i = 1; TRUE; $i++) {
    if (isset($input['title' . $i]) && isset($input['url' . $i])) {
      $sections[] = socialvote_options_section_form_submit($input, $i);
    }
    else {
      break;
    }
  }
  variable_set('socialvote_sections', implode(SOCIALVOTE_SECTION_SPR2, $sections));

  variable_set('socialvote_section_x', $input['section_x']);
  variable_set('socialvote_section_y', $input['section_y']);

  drupal_flush_all_caches();
}

/**
 * Sections part submit.
 */
function socialvote_options_section_form_submit($input, $num) {
  return $input['title' . $num] . SOCIALVOTE_SECTION_SPR1 . $input['url' . $num];
}
