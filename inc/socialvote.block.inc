<?php
/**
 * @file
 * Block
 */

/**
 * Implements hook_block_info().
 */
function socialvote_block_info() {
  return array(
    'socialvote_login_js' => array(
      'info' => 'Socialvote module. ' . t('Log in via network services for voting'),
    ),
    'socialvote_add' => array(
      'info' => 'Socialvote module. ' . t('Form for add new poll'),
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function socialvote_block_view($delta = '') {
  switch ($delta) {
    case 'socialvote_login_js':
      return array(
        'subject' => 'Log in',
        'content' => array(
          '#theme' => 'socialvote_login_js',
        ),
      );

    case 'socialvote_add':
      return array(
        'subject' => t('Add poll'),
        'content' => drupal_get_form('socialvote_form'),
      );

    default:
      return array();
  }
}
