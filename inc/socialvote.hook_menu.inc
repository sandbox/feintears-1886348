<?php
/**
 * @file
 * Menu
 */

/**
 * Implements hook_menu().
 */
function socialvote_menu() {
  $items['admin/reports/socialvote'] = array(
    'title' => 'Log of social votes',
    'page callback' => 'socialvote_log',
    'access arguments' => array('access site reports'),
  );

  if (defined('SOCIALVOTE_PAGELINK_ALL') && defined('SOCIALVOTE_PAGETITLE_ALL')) {

    // Look at inc/socialvote.define.inc.
    $items[SOCIALVOTE_PAGELINK_ALL] = array(
      'title' => SOCIALVOTE_PAGETITLE_ALL,
      'page callback' => 'socialvotes_render',
      'access arguments' => array('access content'),
      'page arguments' => array(SOCIALVOTE_ALL, 1, 2),
      'type' => MENU_CALLBACK,
      'menu_name' => 'main-menu',
    );
  }

  if (defined('SOCIALVOTE_PAGELINK_PUBLISH') && defined('SOCIALVOTE_PAGETITLE_PUBLISH')) {

    // Look at inc/socialvote.define.inc.
    $items[SOCIALVOTE_PAGELINK_PUBLISH] = array(
      'title' => SOCIALVOTE_PAGETITLE_PUBLISH,
      'page callback' => 'socialvotes_render',
      'access arguments' => array('access content'),
      'page arguments' => array(SOCIALVOTE_PUBLISH, 1, 2),
      'type' => MENU_CALLBACK,
      'menu_name' => 'main-menu',
    );
  }

  if (defined('SOCIALVOTE_PAGELINK_NOTPUBLISH') && defined('SOCIALVOTE_PAGELINK_NOTPUBLISH')) {

    // Look at inc/socialvote.define.inc.
    $items[SOCIALVOTE_PAGELINK_NOTPUBLISH] = array(
      'title' => SOCIALVOTE_PAGETITLE_NOTPUBLISH,
      'page callback' => 'socialvotes_render',
      'access arguments' => array('access content'),
      'page arguments' => array(SOCIALVOTE_NOTPUBLISH, 1, 2),
      'type' => MENU_CALLBACK,
      'menu_name' => 'main-menu',
    );
  }

  $items[SOCIALVOTE_PAGELINK_ADDVOTE] = array(
    'title' => SOCIALVOTE_PAGETITLE_ADDVOTE,
    'page callback' => 'socialvote_addvote_render',
    'access arguments' => array('access content'),
    'page arguments' => array(),
    'type' => MENU_CALLBACK,
    'menu_name' => 'main-menu',
  );

  $items['admin/config/content/socialvote'] = array(
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('socialvote_options_form'),
    'title' => 'Socialvote options',
    'description' => 'Configure the settings for Socialvote.',
  );

  return $items;
}

/**
 * For admin/reports/socialvote callback.
 */
function socialvote_log() {
  $header = array(
    array('data' => t('Date'), 'field' => 'sv_l.timestamp', 'sort' => 'desc'),
    t('Message'),
    array('data' => t('Number of vote'), 'field' => 'sv_l.versnum'),
    t('Link'),
    array('data' => t('IP'), 'field' => 'sv_l.user_ip'),
  );

  $query = db_select('socialvote_log', 'sv_l')->extend('PagerDefault')->extend('TableSort');
  $query->leftJoin('socialvote', 'sv', 'sv_l.svid = sv.svid');
  $query
    ->fields('sv_l', array('vid',
      'version',
      'votes',
      'user_id',
      'user_soc_id',
      'user_name',
      'user_ip',
      'timestamp',
      )
    )
    ->fields('sv', array(
      'svid',
      'body',
      'description',
      'section',
      'moderation',
      'api_id',
      )
    );
  $result = $query
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  $rows = array();
  foreach ($result as $row) {
    $sv = entity_create('socialvote', array());
    $sv->svid = $row->svid;
    $sv->section = $row->section;
    $sv->moderation = $row->moderation;
    $sv_uri = socialvote_uri($sv);
    $sv_link = ($row->api_id == 0) ? l($row->body, $sv_uri['path'], array('attributes' => array('target' => '_blank', 'title' => $row->description))) : $row->body;

    $rows[] = array(
      'data' => array(
        format_date($row->timestamp, 'short'),
        t(
          '!name vote for:<br /> @version',
          array(
            '!name' => l(
              $row->user_name,
              socialvote_user_uri($row->user_soc_id, $row->user_id),
              array('attributes' => array('target' => '_blank'))),
            '@version' => $row->version,
          )
        ),
        $row->votes,
        ($sv->svid == '') ? theme_image(
          array(
            'path' => drupal_get_path('module', 'socialvote') . '/images/del.png',
            'width' => 20,
            'height' => 20,
            'alt' => t('deleted'),
            'title' => t('deleted'),
            'attributes' => array(),
          )
        ) : $sv_link,
        $row->user_ip,
      ),
    );
  }

  $build['socialvote_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('Nobody was voted yet.'),
  );
  $build['socialvote_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * The page for add new vote.
 */
function socialvote_addvote_render() {
  return drupal_get_form('socialvote_form');
}
