<?php
/**
 * @file
 * Theme and render arrays.
 */

/**
 * Implements hook_page_alter().
 */
function socialvote_page_alter(&$page) {
  $page['page_top']['socialvote_message'] = array(
    '#theme' => 'socialvote_message',
    '#weight' => 0,
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'socialvote') . '/js/socialvote_message.js'),
      'css' => array(drupal_get_path('module', 'socialvote') . '/css/socialvote_message.css'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function socialvote_theme() {
  return array(
    'socialvote' => array(
      'template' => 'socialvote',
      'variables' => array('sv' => NULL),
      'path' => drupal_get_path('module', 'socialvote') . '/templates',
    ),
    'socialvote_message' => array(
      'template' => 'socialvote_message',
      'variables' => array(),
      'path' => drupal_get_path('module', 'socialvote') . '/templates',
    ),

    'socialvote_ui_overview_item' => array(
      'variables' => array(
        'label' => NULL,
        'entity_type' => NULL,
        'url' => FALSE,
        'name' => FALSE,
      ),
    ),
    'socialvote_field_version' => array(
      'template' => 'socialvote_field_version',
      'render element' => 'element',
      'path' => drupal_get_path('module', 'socialvote') . '/templates/field',
    ),
    'socialvote_options_field_section' => array(
      'template' => 'socialvote_options_field_section',
      'render element' => 'element',
      'path' => drupal_get_path('module', 'socialvote') . '/templates/field',
    ),
    'socialvote_login_js' => array(
      'template' => 'socialvote_login_js',
      'variables' => array('fb' => TRUE, 'vk' => TRUE),
      'path' => drupal_get_path('module', 'socialvote') . '/templates',
    ),
    'socialvotes_section' => array(
      'template' => 'socialvotes_section',
      'variables' => array(
        'svs' => array(),
        'is_all' => FALSE,
        'load' => SOCIALVOTE_ALL,
        'section_id' => 0,
        'section_url' => '',
        'x' => 1,
        'y' => 1,
      ),
      'path' => drupal_get_path('module', 'socialvote') . '/templates/page',
    ),
    'socialvotes_section_navigator' => array(
      'template' => 'socialvotes_section_navigator',
      'variables' => array(
        'sections' => array(),
        'active' => 1,
        'publishlink' => '',
      ),
      'path' => drupal_get_path('module', 'socialvote') . '/templates/page',
    ),
    'socialvotes' => array(
      'template' => 'socialvotes',
      'variables' => array(
        'arr_left' => array(),
        'arr_right' => array(),
        'sv_cur' => NULL,
        'load' => SOCIALVOTE_ALL,
        'section_url' => '',
        'left' => FALSE,
        'right' => TRUE,
      ),
      'path' => drupal_get_path('module', 'socialvote') . '/templates/page',
    ),
    'socialvotes_not_found' => array(
      'template' => 'socialvotes_not_found',
      'variables' => array(),
      'path' => drupal_get_path('module', 'socialvote') . '/templates/page',
    ),
    'socialvote_image' => array(
      'template' => 'socialvote_image',
      'variables' => array(
        'svid' => 0,
        'src' => FALSE,
        'alt' => NULL,
        'title' => NULL,
        'is_cur' => FALSE,
      ),
      'path' => drupal_get_path('module', 'socialvote') . '/templates/page',
    ),
    'socialvote_version' => array(
      'template' => 'socialvote_version',
      'variables' => array(
        'svid' => NULL,
        'vid' => NULL,
        'text' => NULL,
        'votes' => NULL,
      ),
      'path' => drupal_get_path('module', 'socialvote') . '/templates/page',
    ),
  );
}

/**
 * Render array of one socialvote.
 *
 * @param int $sv
 *   entity socialvote
 *
 * @return array
 *   render array
 */
function socialvote_render($sv) {
  return array(
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'socialvote') . '/js/socialvote.js'),
      'css' => array(drupal_get_path('module', 'socialvote') . '/css/socialvote.css'),
    ),
    '#sv' => $sv,
    '#theme' => 'socialvote',
  );
}

/**
 * Page admin/structure/socialvotes item callback.
 */
function theme_socialvote_ui_overview_item($variables) {
  return $variables['label'];
}

/**
 * Section of socialvotes.
 */
function socialvotes_section_render($load, $section_id) {
  $cur_sv = socialvote_load_current($load, $section_id);
  if (isset($cur_sv->svid) && $cur_sv->svid) {
    $svs = socialvote_load_multiple_portion(0, $load, $section_id, $cur_sv->svid, SOCIALVOTE_SECTION_X * SOCIALVOTE_SECTION_Y + 1);
    $is_all = TRUE;
    if (count($svs) == SOCIALVOTE_SECTION_X * SOCIALVOTE_SECTION_Y + 1) {
      end($svs);
      unset($svs[key($svs)]);
      $is_all = FALSE;
    }
    foreach ($svs as $sv) {
      $url_arr = socialvote_uri(
        entity_create(
          'socialvote',
          array(
            'svid' => $sv->svid,
            'section' => $sv->section,
            'moderation' => $sv->moderation,
          )
        )
      );
      $sv->url = $url_arr['path'];
    }
  }
  else {
    $svs = array();
    $is_all = TRUE;
  }

  global $_socialvote_sections;
  $section_url = $_socialvote_sections[$section_id]['url'];

  return array(
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'socialvote') . '/js/socialvote.js'),
      'css' => array(drupal_get_path('module', 'socialvote') . '/css/socialvote_section.css'),
    ),
    '#svs' => $svs,
    '#is_all' => $is_all,
    '#load' => $load,
    '#section_id' => $section_id,
    '#section_url' => $section_url,
    '#x' => SOCIALVOTE_SECTION_X,
    '#y' => SOCIALVOTE_SECTION_Y,
    '#theme' => 'socialvotes_section',
  );
}

/**
 * Section navigator.
 */
function socialvotes_section_navigator_render($publishlink, $active = 1) {
  global $_socialvote_sections;
  return array(
    '#sections' => $_socialvote_sections,
    '#publishlink' => $publishlink,
    '#active' => $active,
    '#theme' => 'socialvotes_section_navigator',
  );
}

/**
 * Render array of "socialvotes".
 */
function socialvotes_render($load = SOCIALVOTE_ALL, $section_url = NULL, $svid = 0) {
  if ($section_url == NULL) {
    return socialvote_notfound_render();
  }
  else {
    global $_socialvote_sections;
    $section_id = -1;
    foreach ($_socialvote_sections as $key => $sect) {
      if ($sect['url'] == $section_url) {
        $section_id = $key;
      }
    }
    if ($section_id == -1) {
      return socialvote_notfound_render();
    }
  }

  if ($svid == NULL) {
    // Current section by default.
    return socialvotes_section_render($load, $section_id);
  }
  else {
    $cur_sv = socialvote_load($svid);
    if (isset($cur_sv->moderation)) {
      if ($load != SOCIALVOTE_ALL && $load != $cur_sv->moderation) {
        $cur_sv = FALSE;
      }
    }
  }

  if ($cur_sv === FALSE || $cur_sv->section != $section_id) {
    return socialvote_notfound_render();
  }

  $portion = SOCIALVOTE_LOADPORTION + 2;

  $arr_left = socialvote_load_multiple_portion(1, $load, $section_id, $svid, $portion);
  if (count($arr_left) == $portion) {
    end($arr_left);
    unset($arr_left[key($arr_left)]);
    $is_left = TRUE;
  }
  else {
    $is_left = FALSE;
  }

  $arr_right = socialvote_load_multiple_portion(0, $load, $section_id, $svid, $portion);
  if (count($arr_right) == $portion) {
    end($arr_right);
    unset($arr_right[key($arr_right)]);
    $is_right = TRUE;
  }
  else {
    $is_right = FALSE;
  }

  $cur_sv->versarr = socialvote_getarrvers($cur_sv->versions);
  return array(
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'socialvote') . '/js/socialvote.js'),
      'css' => array(drupal_get_path('module', 'socialvote') . '/css/socialvote.css'),
    ),
    '#arr_left' => $arr_left,
    '#arr_right' => $arr_right,
    '#sv_cur' => $cur_sv,
    '#load' => $load,
    '#section_url' => $section_url,
    '#left' => $is_left,
    '#right' => $is_right,
    '#theme' => 'socialvotes',
  );
}

/**
 * Socialvote 404.
 */
function socialvote_notfound_render() {
  return array(
    '#attached' => array(
      'css' => array(
        drupal_get_path('module', 'socialvote') . '/css/socialvote.css',
      ),
    ),
    '#theme' => 'socialvotes_not_found',
  );
}

/**
 * Looking socialvote from panel.
 */
function socialvote_image_render($svid, $src, $alt, $title, $is_cur = FALSE) {
  return array(
    '#svid' => $svid,
    '#src' => $src,
    '#alt' => $alt,
    '#title' => $title,
    '#is_cur' => $is_cur,
    '#theme' => 'socialvote_image',
  );
}

/**
 * Version for vote.
 */
function socialvote_version_render($svid, $vid, $text, $votes) {
  return array(
    '#svid' => $svid,
    '#vid' => $vid,
    '#text' => $text,
    '#votes' => $votes,
    '#theme' => 'socialvote_version',
  );
}

/**
 * Transform string for js.
 */
function socialvote_strtostr(array $arr) {
  foreach ($arr as $key => $val) {
    $val = str_replace('\\', '\\\\', $val);
    $val = str_replace('\'', '\\\'', $val);
    $val = str_replace('"', '\\"', $val);
    $val = str_replace("\r\n", '<br />', $val);
    $val = str_replace("\n", '<br />', $val);
    $arr[$key] = $val;
  }
  return $arr;
}
