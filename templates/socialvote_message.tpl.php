<?php
/**
 * @file
 * JS-message of socialvote module.
 */
?>

<div id="socialvote_opacity"></div>
<div id="socialvote_message_wrapper">
  <div id="socialvote_message">
    <div id="socialvote_message_container"></div>
    <div class="sv_del">
      <?php
        $img = theme_image(array(
          'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/del.png',
          'alt' => 'del',
          'title' => t('Close'),
          'width' => '37px',
          'height' => '37px',
          'attributes' => array(),
        ));
        print l($img, '', array(
            'html' => TRUE,
            'fragment' => 'sv',
            'external' => TRUE,
            'attributes' => array('onclick' => 'socialvote_message_close();'),
          )
        );
      ?>
    </div>
  </div>
</div>

<div id="socialvote_loading">
  <?php
    print theme_image(array(
      'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/loading.gif',
      'alt' => 'loading',
      'title' => '',
      'width' => '100px',
      'height' => '100px',
      'attributes' => array(),
    ));
  ?>
</div>
