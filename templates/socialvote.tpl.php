<?php
/**
 * @file
 * One socialvote.
 */
?>

<br />
<?php
  $variables = array(
    'path' => $sv->imagefile,
    'alt' => $sv->body,
    'title' => $sv->body,
    'width' => '100px',
    'height' => '100px',
    'attributes' => array(),
  );
  $img = theme_image($variables);
  print l($img, socialvote_url($sv), array('html' => TRUE));
?>
<br /><br />
<?php $sv->versarr = socialvote_getarrvers($sv->versions); ?>
