<?php
/**
 * @file
 * Looking socialvotes-panel.
 */
?>

<!-- invisible block for login -->
<div id="sv_fixedlogin">
  <?php
    $variables = array(
      'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/login/fb.png',
      'alt' => 'fb',
      'title' => 'facebook.com',
      'width' => '50px',
      'height' => '50px',
      'attributes' => array(),
    );
    print l(theme_image($variables), '', array(
        'html' => TRUE,
        'fragment' => 'sv',
        'external' => TRUE,
        'attributes' => array('onclick' => 'socialvote_message_close(); fb.login();', 'class' => 'fb'),
      )
    );

    $variables = array(
      'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/login/vk.png',
      'alt' => 'vk',
      'title' => 'vk.com',
      'width' => '50px',
      'height' => '50px',
      'attributes' => array(),
    );
    print l(theme_image($variables), '', array(
        'html' => TRUE,
        'fragment' => 'sv',
        'external' => TRUE,
        'attributes' => array('onclick' => 'socialvote_message_close(); vk.login();', 'class' => 'vk'),
      )
    );
  ?>
</div>
<!-- /invisible block for login -->

<!-- invisible template of some elements for jQuery -->
<div style="display: none;">
  <?php $sv_module_path = '/' . drupal_get_path('module', 'socialvote'); ?>

  <span class="sv_vers_templ"><!-- versions -->
    <?php print drupal_render(socialvote_version_render(0, 0, '', '')); ?>
  </span>
  <span class="sv_img_templ_curyes"><!-- image of current sv with link -->
    <?php print drupal_render(socialvote_image_render(0, $sv_module_path . '/images/qm.jpg', NULL, NULL, TRUE)); ?>
  </span>
  <span class="sv_img_templ_curno"><!-- image of not current sv with link -->
    <?php print drupal_render(socialvote_image_render(0, $sv_module_path . '/images/qm.jpg', NULL, NULL, FALSE)); ?>
  </span>
  <span class="sv_img_templ_curno_wl"><!-- image of not current sv without link -->
    <?php print drupal_render(socialvote_image_render(0, FALSE, NULL, NULL, FALSE)); ?>
  </span>

</div>
<!--/invisible template of some elements for jQuery -->


<script type="text/javascript">
<!--//--><![CDATA[//><!--
  //options
  var sv_domain = '<?php print $GLOBALS['base_root']; ?>';
  var sv_files_path = '<?php print variable_get('file_directory_path', conf_path() . '/files'); ?>';
  var sv_module_path = '<?php print $sv_module_path; ?>';
  var sv_load = '<?php print $load; ?>';
  var sv_section_id = <?php print $sv_cur->section; ?>;
  var sv_section_url = '<?php print $section_url; ?>';
  var sv_displayportion_left = <?php print SOCIALVOTE_DISPLAYPORTION_LEFT; ?>;
  var sv_displayportion_right = <?php print SOCIALVOTE_DISPLAYPORTION_RIGHT; ?>;
  var sv_spr1 = '<?php print SOCIALVOTE_VERSION_SPR1; ?>';
  var sv_spr2 = '<?php print SOCIALVOTE_VERSION_SPR2; ?>';
  var sv_pagelink_all = '<?php print defined('SOCIALVOTE_PAGELINK_ALL') ? SOCIALVOTE_PAGELINK_ALL : ''; ?>';
  var sv_pagelink_publish = '<?php print defined('SOCIALVOTE_PAGELINK_PUBLISH') ? SOCIALVOTE_PAGELINK_PUBLISH : ''; ?>';
  var sv_pagelink_notpublish = '<?php print defined('SOCIALVOTE_PAGELINK_NOTPUBLISH') ? SOCIALVOTE_PAGELINK_NOTPUBLISH : ''; ?>';

  //translated by t()
  var sv_vote_templ = '<?php print t('%vote% (I think that \"%version%\")'); ?>';
  var sv_vote_like = '<?php print t('I like this vote'); ?>';
  var sv_vote_thanks = '<?php print t('Thanks for vote.'); ?>';
  var sv_vote_error1 = '<?php print t('Incorrect id of vote or version. Let reload page and try again.'); ?>';
  var sv_vote_error2 = '<?php print t('Error of authorization.'); ?>';
  var sv_vote_error3 = '<?php print t('You was already voted.'); ?>';

  //generate start js-array of socialvotes
  var sv_is_left = <?php print $left ? 'true' : 'false'; ?>;
  var svs_left = [<?php
    foreach($arr_left as $item) :
      $arr_left2[] = 'new socialvote(\'' . implode('\', \'', socialvote_strtostr((array) $item)) . '\')';
    endforeach;
    print implode(', ', $arr_left2);
  ?>];
  svs_left = svs_left.reverse();

  var sv_is_right = <?php print $right ? 'true' : 'false'; ?>;
  var svs_right = [<?php
    foreach($arr_right as $item) :
      $arr_right2[] = 'new socialvote(\'' . implode('\', \'', socialvote_strtostr((array) $item)) . '\')';
    endforeach;
    print implode(', ', $arr_right2);
  ?>];
  var sv_cur = svs_right[0];
  svs_right.splice(0, 1);

  var svs = svs_left.concat(svs_right);//ready
//--><!]]>
</script>




<!-- socialvote -->
<div id="sv">

  <div class="anv"><?php print l(t('Add new vote'), $GLOBALS['base_root'] . '/' . SOCIALVOTE_PAGELINK_ADDVOTE); ?></div>

  <div id="sv_container">
  <!-- left arrow -->
  <div id="sv_left"<?php if(count($arr_left) == 1): ?> style="visibility: hidden;"<?php endif; ?>>
    <?php
      $variables = array(
        'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/left-arrow.png',
        'alt' => 'left',
        'title' => '',
        'width' => '25px',
        'height' => '25px',
        'attributes' => array(),
      );
      print l(theme_image($variables), '', array(
          'html' => TRUE,
          'fragment' => 'sv',
          'external' => TRUE,
          'attributes' => array('onclick' => 'socialvote_left();'),
        )
      );
    ?>
  </div>
  <div id="sv_left_loading" style="visibility: hidden;">
    <?php
    print theme_image(array(
      'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/loading.gif',
      'alt' => 'loading',
      'title' => '',
      'width' => '25px',
      'height' => '25px',
      'attributes' => array(),
    ));
    ?>
  </div>
  <!-- /left arrow -->

  <div class="imagenav">
    <?php
      $disparr = array();

      reset($arr_left);
      // From arr_left.
      for($i = 0; $i < SOCIALVOTE_DISPLAYPORTION_LEFT + 1; $i++) :
        $item = current($arr_left);
        $item->is_cur = ($i == 0);
        array_unshift($disparr, $item);
        next($arr_left);
      endfor;

      reset($arr_right);
      // From arr_right.
      next($arr_right);
      for($i = 0; $i < SOCIALVOTE_DISPLAYPORTION_RIGHT; $i++) :
        $item = current($arr_right);
        $item->is_cur = FALSE;
        $disparr[] = $item;
        next($arr_right);
      endfor;

      foreach($disparr as $item) :
        if(isset($item->svid)) :
          print drupal_render(socialvote_image_render($item->svid, $item->imagefile, $item->description, $item->body, $item->is_cur));
        else :
          print drupal_render(socialvote_image_render(0, FALSE, '', '', FALSE));
        endif;
      endforeach;
    ?>
  </div>

  <!-- right arrow -->
  <div id="sv_right_loading" style="visibility: hidden;">
    <?php
      print theme_image(array(
        'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/loading.gif',
        'alt' => 'loading',
        'title' => '',
        'width' => '25px',
        'height' => '25px',
        'attributes' => array(),
      ));
    ?>
  </div>
  <div id="sv_right"<?php if(count($arr_right) == 1): ?> style="visibility: hidden;"<?php endif; ?>>
    <?php
    $variables = array(
      'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/right-arrow.png',
      'alt' => 'right',
      'title' => '',
      'width' => '25px',
      'height' => '25px',
      'attributes' => array(),
    );
    print l(theme_image($variables), '', array(
      'html' => TRUE,
      'fragment' => 'sv',
      'external' => TRUE,
      'attributes' => array('onclick' => 'socialvote_right();'),
      )
    );
    ?>
  </div>
  <!-- /right arrow -->
  </div>
  <!-- /sv_container -->


  <div class="vote">
    <!-- <span class="svrep_svid"><?php print $sv_cur->svid; ?></span> : -->
    <span class="svrep_body"><?php print $sv_cur->body; ?></span>
    <br />
    <span class="svrep_description"><?php print $sv_cur->description; ?></span>
    <div>
      <?php
      $variables = array(
        'path' => $sv_cur->imagefile,
        'alt' => $sv_cur->description,
        'title' => $sv_cur->body,
        'width' => '450px',
        'height' => '450px',
        'attributes' => array('class' => 'svrep_imagefile'),
      );
      print theme_image($variables);
    ?>
    </div>

  </div>

  <div class="versions">
    <?php
    foreach($sv_cur->versarr as $key => $vers) :

      print drupal_render(socialvote_version_render($sv_cur->svid, $key, $vers['text'], $vers['votes']));

    endforeach;
    ?>
  </div>

</div>
<!-- /socialvote -->

<!-- like -->
<div id="sv_like">
    <?php
    $sv = socialvote_generate($sv_cur->svid * 100 + 1, $sv_cur->imagefile);
    $versions = explode(SOCIALVOTE_VERSION_SPR2, $sv->versions);
    $vers1 = explode(SOCIALVOTE_VERSION_SPR1, $versions[0]);
    $vers2 = explode(SOCIALVOTE_VERSION_SPR1, $versions[1]);
    ?>
    <script type="text/javascript">
        <!--//--><![CDATA[//><!--
        var sv_gen_id = <?php print $sv->svid; ?>;
        //--><!]]>
    </script>
    <a href="#sv_like" onclick="socialvote_vote_generate(sv_gen_id, 0, sv_vote_like, sv_cur.uri(), sv_cur.getimagefile());"><?php print t('I like this'); ?></a> (<span id="sv_likeit"><?php print $vers1[1]; ?></span>)
    <hr />
</div>
<!-- /like -->


<!-- social comments -->
<div id="sv_comments">
    <div class="sv_a">
        <a class="vk" href="#sv_comments" onclick="social.getSocById('vk').comments('#sv_comments .sv_comments', sv_cur.uri());">VK</a>
        <a class="fb" href="#sv_comments" onclick="social.getSocById('fb').comments('#sv_comments .sv_comments', sv_cur.uri());">Facebook</a>
    </div>
    <div class="sv_comments"></div>
</div>
<!-- /social comments -->
