<?php
/**
 * @file
 * Version for vote.
 */
?>

<div class="vn">
  <span class="svrep_text"><?php print $text; ?></span>&nbsp;&nbsp;<?php
    $img = theme_image(array(
        'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/handup.jpg',
        'alt' => 'hand up',
        'title' => '',
        'width' => '20px',
        'height' => '20px',
        'attributes' => array(),
    ));
    print l($img . '<span class="svrep_votes">' . $votes . '</span>', '', array(
        'html' => TRUE,
        'fragment' => 'sv',
        'external' => TRUE,
        'attributes' => array(
          'svid' => $svid,
          'vid' => $vid,
          'class' => 'vote',
          'onclick' => 'socialvote_vote(jQuery(this).attr(\'svid\'), jQuery(this).attr(\'vid\'));',
        ),
      )
    );
  ?>
</div>
