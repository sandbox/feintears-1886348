<?php
/**
 * @file
 * Section of socialvotes.
 */
?>

<div id="sv_section">
<div class="anv"><?php print l(t('Add new vote'), $GLOBALS['base_root'] . '/' . SOCIALVOTE_PAGELINK_ADDVOTE); ?></div>
<?php if(empty($svs)): ?>
    <div><?php print t('This section has no socialvotes yet'); ?></div>
<?php endif; ?>
<?php for($j = 0; $j < $y; $j++): ?>
  <div class="sv_row">
  <?php for($i = 0; $i < $x; $i++): ?>

    <?php $sv = current($svs); ?>
    <?php if($sv) : ?>
      <div class="sv_section_one">
        <div>&nbsp;<?php print $sv->body; ?></div>
        <div><?php
          $theme_image = theme_image(array(
            'path' => $sv->imagefile,
            'alt' => 'socialvote image',
            'title' => $sv->description,
            'width' => '200px',
            'height' => '200px',
            'attributes' => array(),
          ));
          $l = l($theme_image, $sv->url, array('html' => TRUE));
          print $l;
        ?></div>
        <div>&nbsp;<?php print $sv->description; ?></div>
      </div>
    <?php endif; ?>
    <?php next($svs); ?>

  <?php endfor; ?>
  </div>
<?php endfor; ?>
</div>

<?php if(!$is_all): ?>
  <div id="sv_load_section">
    <span class='click'><a href="#sv_load_section" onclick="sv_load_section();"><?php print t('More'); ?></a></span>
    <span class='loading'>
      <?php
        print theme_image(array(
          'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/loading.gif',
          'alt' => 'loading',
          'title' => '',
          'width' => '25px',
          'height' => '25px',
          'attributes' => array(),
        ));
      ?>
    </span>
  </div>
  <script type="text/javascript">
  <!--//--><![CDATA[//><!--
    var sv_domain = '<?php print $GLOBALS['base_root']; ?>';
    var sv_files_path = '<?php print variable_get('file_directory_path', conf_path() . '/files'); ?>';
    var sv_section_url = '<?php print $section_url; ?>';
    var sv_spr1 = '<?php print SOCIALVOTE_VERSION_SPR1; ?>';
    var sv_spr2 = '<?php print SOCIALVOTE_VERSION_SPR2; ?>';
    var sv_pagelink_all = '<?php print defined('SOCIALVOTE_PAGELINK_ALL') ? SOCIALVOTE_PAGELINK_ALL : ''; ?>';
    var sv_pagelink_publish = '<?php print defined('SOCIALVOTE_PAGELINK_PUBLISH') ? SOCIALVOTE_PAGELINK_PUBLISH : ''; ?>';
    var sv_pagelink_notpublish = '<?php print defined('SOCIALVOTE_PAGELINK_NOTPUBLISH') ? SOCIALVOTE_PAGELINK_NOTPUBLISH : ''; ?>';
    var sv_section_lastsvid = <?php end($svs); print $svs[key($svs)]->svid; ?>;
    function sv_load_section() {
      jQuery('#sv_load_section .click').css('visibility', 'hidden');
      jQuery('#sv_load_section .loading').css('visibility', 'visible');
      socialvote_services_load(<?php print $x * $y; ?>, 0, <?php print $load; ?>, <?php print $section_id; ?>,  sv_section_lastsvid, function(data, textStatus, jqXHR) {
          var resp = jQuery.parseJSON(data);
          var arr = [];
          for(i in resp)
            if(i != sv_section_lastsvid && i != 'more') {
              arr.unshift(resp[i]);
            }
          var row = ''; var x=0; var X = <?php print $x; ?>;
          var a = '';
          for(i in arr) {
            x++; row += sv_onesv_section(arr[i]);
            if(x == X) {
              jQuery('#sv_section').append(sv_row_section(row));
              row = '';
            }
            sv_section_lastsvid = arr[i].svid;
          }
          if(row != '')
            jQuery('#sv_section').append(sv_row_section(row));

          jQuery('#sv_load_section .loading').css('visibility', 'hidden');
          if(resp.more == "no") {
            jQuery('#sv_load_section').css('visibility', 'hidden');
          }
          else {
            jQuery('#sv_load_section .click').css('visibility', 'visible');
          }
        },
        function(jqXHR, textStatus, errorThrown) {
          socialvote_connect_error(jqXHR, textStatus, errorThrown);
        });
    }
    function sv_onesv_section(sv) {
      var sv = new socialvote(sv.svid, sv.body, sv.description, sv.versions, sv.versions_count, sv.imagefile, sv.author_uid, sv.author_soc_id, sv.author_name, sv.moderation, sv.section, sv.api_id, sv.created, sv.changed);
      var url = '<a href=\"' + sv.uri() + '\"><img src=\"' + sv.getimagefile() + '\" width=\"200px\" height=\"200px\" alt=\"' + sv.description + '\" title=\"' + sv.description + '\" /></a>';
      return '<div class=\"sv_section_one\"><div>&nbsp;' + sv.body + '</div><div>' + url + '</div><div>&nbsp;' + sv.description + '</div></div>';
    }
    function sv_row_section(data) {
      return '<div class=\"sv_row\">' + data + '</div>';
    }
  //--><!]]>
  </script>
<?php endif; ?>
