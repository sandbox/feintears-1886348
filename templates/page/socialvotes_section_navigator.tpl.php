<?php
/**
 * @file
 * Navigator of sections.
 */
?>

<?php foreach($sections as $key => $sect) : ?>
  <span class="menuitem<?php print ($key == $active) ? ' active-trail' : ''; ?>">
    <?php print l(t($sect['name']), $publishlink . '/' . $sect['url']); ?>
  </span>
<?php endforeach;?>
