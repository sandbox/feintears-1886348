<?php
/**
 * @file
 * Looking socialvote from panel.
 */
?>

<div class="sv_img <?php print $is_cur ? 'cur_yes' : 'cur_no'; ?>">
  <?php
    if($src) :
      $variables = array(
        'path' => $src,
        'alt' => $alt,
        'title' => $title,
        'width' => '100px',
        'height' => '100px',
        'attributes' => array('class' => 'svrep_imagefile'),
      );
      print l(theme_image($variables), '', array(
          'html' => TRUE,
          'fragment' => 'sv',
          'external' => TRUE,
          'attributes' => array(
            'svid' => $svid,
            'class' => 'svrep_a',
            'onclick' => 'socialvote_current_svid(jQuery(this).attr(\'svid\'));',
          ),
        )
      );
    else :
      $variables = array(
        'path' => drupal_get_path('module', 'socialvote') . '/images/transparent.png',
        'alt' => t('indefinitely'),
        'title' => t('indefinitely'),
        'width' => '100px',
        'height' => '100px',
        'attributes' => array('class' => 'svrep_imagefile'),
      );
      print theme_image($variables);
    endif;
  ?>
</div>
