<?php
/**
 * @file
 * Version input type=number.
 */
?>

<div class="form-item form-type-textfield form-item-version">
  <div class="label"><label for="<?php print $element['#id']; ?>"><?php print $element['#title']; ?><?php if($element['#required']): ?> <span class="form-required" title="<?php print t('This field is required.'); ?>">*</span><?php endif; ?></label></div>
  <div class="input">
    <?php if(isset($element['#base_type']) && $element['#base_type'] == 'number'): ?>
      <input class="sv_countversion form-text" type="number" id="<?php print $element['#id']; ?>" name="<?php print $element['#name']; ?>" value="0" size="<?php print $element['#size']; ?>" maxlength="<?php print $element['#maxlength']; ?>" min="0" style="width: 50px;" />
    <?php else:
      print $element['#children'];
      endif;
    ?>
  </div>
</div>
