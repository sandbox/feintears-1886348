<?php
/**
 * @file
 * Block for login.
 */
?>

<div id="login">
  <div id="social-login" style="visibility: hidden;">
    <div class="text"><?php print t('Login via'); ?></div>
    <div class="btns">

      <?php if($fb) : ?>
        <a href="#" onclick="onlogin_svid = null; onlogin_vid = null; fb.login();">
        <?php
          print theme_image(array(
            'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/login/fb.png',
            'alt' => '',
            'title' => 'facebook',
            'width' => '35px',
            'height' => '35px',
            'attributes' => array(),
          ));
        ?>
        </a>
      <?php endif; ?>

      <?php if($vk) : ?>
        <a href="#" onclick="onlogin_svid = null; onlogin_vid = null; vk.login();">
        <?php
          print theme_image(array(
            'path' => '/' . drupal_get_path('module', 'socialvote') . '/images/login/vk.png',
            'alt' => '',
            'title' => 'vkontakte',
            'width' => '35px',
            'height' => '35px',
            'attributes' => array(),
          ));
        ?>
        </a>
      <?php endif; ?>

    </div>
  </div>

  <div id="social-logout" style="visibility: hidden;">

    <?php if($fb) : ?>
      <div id="fb" style="visibility: hidden;">
        <div class="text"><span class="hello"></span></div>
        <div class="btns"><a href="#" onclick="fb.logout();"><?php print t('logout'); ?></a></div>
      </div>
    <?php endif; ?>

    <?php if($vk) : ?>
      <div id="vk" style="visibility: hidden;">
        <div class="text"><span class="hello"></span></div>
        <div class="btns"><a href="#" onclick="vk.logout();"><?php print t('logout'); ?></a></div>
      </div>
    <?php endif; ?>

  </div>
</div><!-- /.login -->
