<?php
/**
 * @file
 * Rules
 */

/**
 * Implements hook_rules_event_info().
 */
function socialvote_rules_event_info() {
  return array(
    'socialvote_rules_create_sv' => array(
      'label' => t('After creating socialvote'),
      'module' => 'socialvote',
      'group' => 'Social vote',
      'variables' => array(
        'sv_body' => array('type' => 'text', 'label' => t('sv->body')),
        'sv_description' => array('type' => 'text', 'label' => t('sv->description')),
        'sv_url' => array('type' => 'text', 'label' => t('Created socialvote url')),
      ),
    ),
    'socialvote_rules_vote' => array(
      'label' => t('After every voting'),
      'module' => 'socialvote',
      'group' => 'Social vote',
      'variables' => array(
        'voter_name' => array('type' => 'text', 'label' => t('Name of voter in social services')),
        'voter_url' => array('type' => 'text', 'label' => t('Url of voter in social services')),
        'sv_url' => array('type' => 'text', 'label' => t('Socialvote url')),
        'version_text' => array('type' => 'text', 'label' => t('Text of version')),
      ),
    ),
  );
}
